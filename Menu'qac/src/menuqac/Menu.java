package menuqac;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import menuqac.menu.Section;

public class Menu implements Serializable {
	private static final long serialVersionUID = 1L;

	private List<Section> sections;

	public Menu() {
		this.sections = new LinkedList<Section>();
		parse();
	}

	public void parse() {
		try {
			FileReader fr = new FileReader(new File("data/Menu.txt"));
			BufferedReader br = new BufferedReader(fr);
			ArrayList<String> strings = new ArrayList<String>();
			StringBuilder temp = new StringBuilder();
			;
			while (br.ready()) {
				String s = br.readLine();
				temp.append(s);
				if (s.contains("}")) {
					strings.add(temp.toString());
					temp.setLength(0);
				}
			}
			for (String s : strings) {
				String[] tab = s.split("\\{");
				String parent = tab[0];
				parseChildren(parent, tab[1]);
			}
			br.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void parseChildren(String parent, String s) {
		if (parent.matches("[Dd][Rr][Ii][Nn][Kk][Ss]*")) {
			String[] tab = s.split("[()]");
			for (int i = 0; i < tab.length - 1; i += 2) {
				this.sections.add(new Section("Drinks", tab[i].trim(), tab[i + 1]));
			}
		} else {
			String[] list = s.split("\\]");
			for (String str : list) {
				String[] tab = str.split("\\[");
				if (tab.length > 1) {
					String p = tab[0].trim();
					String[] listP = tab[1].split("[()]");
					for (int i = 0; i < listP.length - 1; i += 2) {
						this.sections.add(new Section(p, listP[i].trim(), listP[i + 1]));
					}
				}
			}
		}
	}

	public List<Section> getSections() {
		return this.sections;
	}

	public List<Section> getChildFromParent(String parent) {
		List<Section> list = new LinkedList<Section>();
		for (Section s : this.sections) {
			if (s.getParent().equals(parent.trim())) {
				list.add(s);
			}
		}
		return list;
	}

	public static void main(String[] args0) {
		Menu m = new Menu();
		System.out.println(m.getSections());
	}

	public String toString() {
		return this.sections.toString();
	}
}