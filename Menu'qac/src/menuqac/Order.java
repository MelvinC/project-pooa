package menuqac;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;
import menuqac.prototype.Food;
import menuqac.prototype.drink.ColdDrinks;
import menuqac.prototype.drink.HotDrinks;
import menuqac.prototype.food.mainDishes.Meats;

public class Order implements Serializable {
	private static final long serialVersionUID = 1L;

	private int number;
	private int waiter;
	private int table;
	private List<Prototype> prototypes;

	public Order(int number, int waiter, int table) {
		this.number = number;
		this.waiter = waiter;
		this.table = table;
		this.prototypes = new LinkedList<Prototype>();
	}

	public float getPrice() {
		float result = 0;
		for (Prototype p : this.prototypes) {
			result += p.getPrice();
		}
		return result;
	}

	public void add(Prototype p) {
		this.prototypes.add(p);
	}

	public List<Prototype> get() {
		return this.prototypes;
	}

	public List<Prototype> getPrototypes() {
		List<Prototype> list = new LinkedList<Prototype>();
		for (int i = 0; i < this.prototypes.size(); i++) {
			if (this.prototypes.get(i).getQuantity() > 0) {
				boolean added = false;
				for (int j = 0; j < list.size(); j++) {
					if (this.prototypes.get(i) instanceof Meats && list.get(j) instanceof Meats) {
						Meats m = (Meats) this.prototypes.get(i);
						Meats m2 = (Meats) list.get(j);
						if (m.getName().equals(m2.getName()) && m.getComment().equals(m2.getComment()) && !added
								&& m.getSauce().equals(m2.getSauce()) && m.getCooking().equals(m2.getCooking())) {
							m2.setQuantity(m2.getQuantity() + m.getQuantity());
							added = true;
						}
					} else if (this.prototypes.get(i) instanceof Food && list.get(j) instanceof Food) {
						Food f = (Food) this.prototypes.get(i);
						Food f2 = (Food) list.get(j);
						if (f.getName().equals(f2.getName()) && f.getComment().equals(f2.getComment()) && !added
								&& f.getSauce().equals(f2.getSauce())) {
							f2.setQuantity(f2.getQuantity() + f.getQuantity());
							added = true;
						}
					} else if (this.prototypes.get(i) instanceof ColdDrinks && list.get(j) instanceof ColdDrinks) {
						ColdDrinks cd = (ColdDrinks) this.prototypes.get(i);
						ColdDrinks cd2 = (ColdDrinks) list.get(j);
						if (cd.getName().equals(cd2.getName()) && cd.getComment().equals(cd2.getComment()) && !added
								&& cd.getIceCubes() == cd2.getIceCubes()) {
							cd2.setQuantity(cd2.getQuantity() + cd.getQuantity());
							added = true;
						}
					} else if (this.prototypes.get(i) instanceof HotDrinks && list.get(j) instanceof HotDrinks) {
						HotDrinks hd = (HotDrinks) this.prototypes.get(i);
						HotDrinks hd2 = (HotDrinks) list.get(j);
						if (hd.getName().equals(hd2.getName()) && hd.getComment().equals(hd2.getComment()) && !added
								&& hd.getSugar() == hd2.getSugar()) {
							hd2.setQuantity(hd2.getQuantity() + hd.getQuantity());
							added = true;
						}
					} else if (this.prototypes.get(i).getName().equals(list.get(j).getName())
							&& this.prototypes.get(i).getComment().equals(list.get(j).getComment()) && !added) {
						list.get(j).setQuantity(list.get(j).getQuantity() + this.prototypes.get(i).getQuantity());
						added = true;
					}
				}
				if (!added) {
					list.add(this.prototypes.get(i));
				}
			}
		}
		this.prototypes = list;
		return this.prototypes;
	}

	public void addPrototype(Prototype p) {
		if (this.prototypes.size() > 0) {
			boolean added = false;
			for (int i = 0; i < this.prototypes.size(); i++) {
				if (this.prototypes.get(i) instanceof Meats && p instanceof Meats) {
					Meats m = (Meats) this.prototypes.get(i);
					Meats m2 = (Meats) p;
					if (m.getName().equals(m2.getName()) && m.getComment().equals(m2.getComment()) && !added
							&& m.getSauce().equals(m2.getSauce()) && m.getCooking().equals(m2.getCooking())) {
						m.plus1();
						added = true;
					}
				} else if (this.prototypes.get(i) instanceof Food && p instanceof Food) {
					Food f = (Food) this.prototypes.get(i);
					Food f2 = (Food) p;
					if (f.getName().equals(f2.getName()) && f.getComment().equals(f2.getComment()) && !added
							&& f.getSauce().equals(f2.getSauce())) {
						f.plus1();
						added = true;
					}
				} else if (this.prototypes.get(i) instanceof ColdDrinks && p instanceof ColdDrinks) {
					ColdDrinks cd = (ColdDrinks) this.prototypes.get(i);
					ColdDrinks cd2 = (ColdDrinks) p;
					if (cd.getName().equals(cd2.getName()) && cd.getComment().equals(cd2.getComment()) && !added
							&& cd.getIceCubes() == cd2.getIceCubes()) {
						cd.plus1();
						added = true;
					}
				} else if (this.prototypes.get(i) instanceof HotDrinks && p instanceof HotDrinks) {
					HotDrinks hd = (HotDrinks) this.prototypes.get(i);
					HotDrinks hd2 = (HotDrinks) p;
					if (hd.getName().equals(hd2.getName()) && hd.getComment().equals(hd2.getComment()) && !added
							&& hd.getSugar() == hd2.getSugar()) {
						hd.plus1();
						added = true;
					}
				} else if (this.prototypes.get(i).getName().equals(p.getName())
						&& this.prototypes.get(i).getComment().equals(p.getComment()) && !added) {
					this.prototypes.get(i).plus1();
					added = true;
				}
			}
			if (!added) {
				this.prototypes.add(p);
			}
		} else {
			this.prototypes.add(p);
		}
	}

	public void empty() {
		this.prototypes.clear();
	}

	public int getTableNumber() {
		return this.table;
	}

	public void setTableNumber(int table) {
		this.table = table;
	}

	public int getNumber() {
		return this.number;
	}

	public int getWaiter() {
		return this.waiter;
	}

	public String toString() {
		String str = "";
		for (Prototype p : this.prototypes) {
			str += p.toString() + System.lineSeparator();
		}
		return str;
	}
}