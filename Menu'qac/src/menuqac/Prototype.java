package menuqac;

import java.io.Serializable;
import menuqac.visitor.CommentVisitor;

public class Prototype implements Serializable {
	private static final long serialVersionUID = 1L;

	protected String name;
	protected float price;
	protected int target;
	protected String comment;
	private int quantity;

	private boolean isSelected;
	private boolean payed;

	public Prototype(String _name, float _price, int _target, String _comment) {
		this.name = _name;
		this.price = _price;
		this.target = _target;
		this.comment = _comment;
		this.quantity = 1;
		this.isSelected = true;
		this.payed = false;
	}

	public String getName() {
		return this.name;
	}

	public float getPrice() {
		return this.price;
	}

	public int getTarget() {
		return this.target;
	}

	public String getComment() {
		return this.comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public boolean isSelected() {
		return this.isSelected;
	}

	public void setSelected(boolean isSelected) {
		this.isSelected = isSelected;
	}

	public void payed() {
		this.payed = true;
	}

	public boolean isPayed() {
		return this.payed;
	}

	public void accepts(Visitor v, Object value) {
		if (v instanceof CommentVisitor) {
			this.setComment((String) value);
		} else {
			this.accept(v, value);
		}
	}

	public void accept(Visitor v, Object value) {
		// only for the other visitors -> if concerned, override this method
	}

	public int getQuantity() {
		return this.quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public int plus1() {
		this.quantity++;
		return this.quantity;
	}

	public int minus1() {
		this.quantity--;
		return this.quantity;
	}

	public Prototype clone() {
		return new Prototype(this.name, this.price, this.target, this.comment);
	}

	@Override
	public String toString() {
		String s = "<p style=\"text-align:left; color: black;\">" + this.name + "<br><p>" + this.comment
				+ "</p><br> <p style=\"text-align: right; color: black;\">" + this.price + " �</p></p>";
		return s;
	}
}