package menuqac;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

public class Table implements Serializable {
	private static final long serialVersionUID = 1L;

	private int number;
	private List<Order> orders;

	public Table(int number) {
		this.number = number;
		this.orders = new LinkedList<Order>();
	}

	public float getPrice() {
		float result = 0;
		for (Order o : this.orders) {
			result += o.getPrice();
		}
		return result;
	}

	public List<Prototype> getPrototypes() {
		LinkedList<Prototype> result = new LinkedList<Prototype>();
		for (Order o : orders) {
			result.addAll(o.get());
		}
		return result;
	}

	public int getNumber() {
		return this.number;
	}

	public void addOrder(Order o) {
		this.orders.add(o);
	}

	public void flush() {
		this.orders.clear();
	}

	@Override
	public String toString() {
		return ("Table " + this.number);
	}
}