package menuqac.prototype;

import menuqac.Prototype;

public class Drinks extends Prototype {
	private static final long serialVersionUID = 1L;

	public Drinks(String _name, float _price, int _target, String _comment) {
		super(_name, _price, _target, _comment);
	}
}