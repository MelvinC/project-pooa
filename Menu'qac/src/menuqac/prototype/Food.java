package menuqac.prototype;

import menuqac.Prototype;
import menuqac.Visitor;
import menuqac.visitor.SauceVisitor;
import menuqac.visitor.SauceVisitor.Sauce;

public class Food extends Prototype {
	private static final long serialVersionUID = 1L;

	protected Sauce sauce;

	public Food(String _name, float _price, int _target, String _comment) {
		super(_name, _price, _target, _comment);
		this.sauce = Sauce.NONE;
	}

	public void setSauce(Sauce sauce) {
		this.sauce = sauce;
	}

	public Sauce getSauce() {
		return this.sauce;
	}

	@Override
	public Prototype clone() {
		Food m = new Food(this.name, this.price, this.target, this.comment);
		m.setSauce(this.sauce);
		return m;
	}

	@Override
	public void accept(Visitor v, Object value) {
		if (v instanceof SauceVisitor) {
			this.setSauce((Sauce) value);
		}
	}

	@Override
	public String toString() {
		String s = "<p style=\"text-align:left; color: black;\">" + this.name + "<br>"
				+ (this.sauce == Sauce.NONE ? "" : "<p>" + this.sauce + "</p>")
				+ (this.comment == null ? "" : "<p>" + this.comment + "</p>")
				+ "<p style=\"text-align: right; color: black;\">" + this.price + " �</p></p>";
		return s;
	}
}