package menuqac.prototype.drink;

import menuqac.Prototype;
import menuqac.Visitor;
import menuqac.prototype.Drinks;
import menuqac.visitor.IceCubesVisitor;

public class ColdDrinks extends Drinks {
	private static final long serialVersionUID = 1L;

	private boolean hasIceCubes;

	public ColdDrinks(String _name, float _price, int _target, String _comment) {
		super(_name, _price, _target, _comment);
		this.hasIceCubes = true;
	}

	public void setIceCubes(boolean hasIceCubes) {
		this.hasIceCubes = hasIceCubes;
	}

	public boolean getIceCubes() {
		return this.hasIceCubes;
	}

	@Override
	public void accept(Visitor v, Object value) {
		if (v instanceof IceCubesVisitor) {
			this.setIceCubes((boolean) value);
		}
	}

	@Override
	public Prototype clone() {
		ColdDrinks cd = new ColdDrinks(this.name, this.price, this.target, this.comment);
		cd.setIceCubes(this.hasIceCubes);
		return cd;
	}

	@Override
	public String toString() {
		String s = "<p style=\"text-align:left; color: black;\">" + this.name + "<br><p>"
				+ (this.hasIceCubes ? "w icecubes" : "w/o icecubes") + "</p>"
				+ (this.comment == null ? "" : "<p>" + this.comment + "</p>")
				+ "<p style=\"text-align: right; color: black;\">" + this.price + " �</p></p>";
		return s;
	}
}