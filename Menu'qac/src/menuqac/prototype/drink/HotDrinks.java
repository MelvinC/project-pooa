package menuqac.prototype.drink;

import menuqac.Prototype;
import menuqac.Visitor;
import menuqac.prototype.Drinks;
import menuqac.visitor.SugarVisitor;

public class HotDrinks extends Drinks {
	private static final long serialVersionUID = 1L;

	private boolean hasSugar;

	public HotDrinks(String _name, float _price, int _target, String _comment) {
		super(_name, _price, _target, _comment);
		this.hasSugar = true;
	}

	public void setSugar(boolean hasSugar) {
		this.hasSugar = hasSugar;
	}

	public boolean getSugar() {
		return this.hasSugar;
	}

	@Override
	public void accept(Visitor v, Object value) {
		if (v instanceof SugarVisitor) {
			this.setSugar((boolean) value);
		}
	}

	@Override
	public Prototype clone() {
		HotDrinks hd = new HotDrinks(this.name, this.price, this.target, this.comment);
		hd.setSugar(this.hasSugar);
		return hd;
	}

	@Override
	public String toString() {
		String s = "<p style=\"text-align:left; color: black;\">" + this.name + "<br><p>"
				+ (this.hasSugar ? "w sugar" : "w/o sugar") + "</p>"
				+ (this.comment == null ? "" : "<p>" + this.comment + "</p>")
				+ "<p style=\"text-align: right; color: black;\">" + this.price + " �</p></p>";
		return s;
	}
}