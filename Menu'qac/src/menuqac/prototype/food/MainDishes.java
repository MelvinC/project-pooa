package menuqac.prototype.food;

import menuqac.prototype.Food;

public class MainDishes extends Food {
	private static final long serialVersionUID = 1L;

	public MainDishes(String _name, float _price, int _target, String _comment) {
		super(_name, _price, _target, _comment);
	}
}