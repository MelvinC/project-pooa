package menuqac.prototype.food.desert;

import menuqac.Prototype;
import menuqac.prototype.food.Deserts;

public class UniqueSize extends Deserts {
	private static final long serialVersionUID = 1L;

	public UniqueSize(String _name, float _price, int _target, String _comment) {
		super(_name, _price, _target, _comment);
	}

	@Override
	public Prototype clone() {
		UniqueSize m = new UniqueSize(this.name, this.price, this.target, this.comment);
		m.setSauce(this.sauce);
		return m;
	}
}