package menuqac.prototype.food.desert;

import menuqac.Prototype;
import menuqac.prototype.food.Deserts;

public class VariableSize extends Deserts {
	private static final long serialVersionUID = 1L;

	public VariableSize(String _name, float _price, int _target, String _comment) {
		super(_name, _price, _target, _comment);
	}

	@Override
	public Prototype clone() {
		VariableSize m = new VariableSize(this.name, this.price, this.target, this.comment);
		m.setSauce(this.sauce);
		return m;
	}
}