package menuqac.prototype.food.mainDishes;

import menuqac.Prototype;
import menuqac.prototype.food.MainDishes;

public class Commons extends MainDishes {
	private static final long serialVersionUID = 1L;

	public Commons(String _name, float _price, int _target, String _comment) {
		super(_name, _price, _target, _comment);
	}

	@Override
	public Prototype clone() {
		Commons m = new Commons(this.name, this.price, this.target, this.comment);
		m.setSauce(this.sauce);
		return m;
	}
}