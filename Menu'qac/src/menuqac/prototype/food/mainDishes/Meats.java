package menuqac.prototype.food.mainDishes;

import menuqac.Prototype;
import menuqac.Visitor;
import menuqac.prototype.food.MainDishes;
import menuqac.visitor.CookingVisitor;
import menuqac.visitor.SauceVisitor;
import menuqac.visitor.CookingVisitor.Cooking;
import menuqac.visitor.SauceVisitor.Sauce;

public class Meats extends MainDishes {
	private static final long serialVersionUID = 1L;

	private Cooking cooking;

	public Meats(String _name, float _price, int _target, String _comment) {
		super(_name, _price, _target, _comment);
		this.cooking = Cooking.MR;
	}

	public void setCooking(Cooking cooking) {
		this.cooking = cooking;
	}

	public Cooking getCooking() {
		return this.cooking;
	}

	@Override
	public void accept(Visitor v, Object value) {
		if (v instanceof SauceVisitor) {
			this.setSauce((Sauce) value);
		} else if (v instanceof CookingVisitor) {
			this.setCooking((Cooking) value);
		}
	}

	@Override
	public Prototype clone() {
		Meats m = new Meats(this.name, this.price, this.target, this.comment);
		m.setCooking(this.cooking);
		m.setSauce(this.sauce);
		return m;
	}

	@Override
	public String toString() {
		String s = "<p style=\"text-align:left; color: black;\">" + this.name + "<br><p>" + this.cooking
				+ (this.sauce == Sauce.NONE ? "" : " " + this.sauce) + "</p>"
				+ (this.comment == null ? "" : "<p>" + this.comment + "</p>")
				+ "<p style=\"text-align: right; color: black;\">" + this.price + " �</p></p>";
		return s;
	}
}