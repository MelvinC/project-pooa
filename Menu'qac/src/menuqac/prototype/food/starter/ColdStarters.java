package menuqac.prototype.food.starter;

import menuqac.Prototype;
import menuqac.prototype.food.Starters;

public class ColdStarters extends Starters {
	private static final long serialVersionUID = 1L;

	public ColdStarters(String _name, float _price, int _target, String _comment) {
		super(_name, _price, _target, _comment);
	}

	@Override
	public Prototype clone() {
		ColdStarters m = new ColdStarters(this.name, this.price, this.target, this.comment);
		m.setSauce(this.sauce);
		return m;
	}
}