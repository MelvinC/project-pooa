package menuqac.ui;

import java.awt.Color;
import java.awt.Font;
import javax.swing.JLabel;
import javax.swing.JPanel;
import menuqac.menu.Section;

public class Activity extends JPanel {
	private static final long serialVersionUID = 1L;

	private CheckBox cb;

	public Activity(Section s, int w, int y, boolean b) {
		this.setBackground(Color.WHITE);
		this.setAlignmentX(JPanel.LEFT_ALIGNMENT);
		this.setLayout(null);
		this.setBounds(0, y, w - 14, 50);

		CheckBox cb = new CheckBox(w / 6, 15, 25, 15, b);
		this.cb = cb;
		this.add(cb);

		JLabel label = new JLabel(s.getName());
		label.setFont(new Font("Dialog", Font.BOLD, 15));
		label.setBounds(w / 6 + 25, 15, 200, 15);
		this.add(label);
	}

	public CheckBox getCheckBox() {
		return this.cb;
	}
}