package menuqac.ui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.MouseEvent;
import java.awt.geom.Rectangle2D;
import javax.swing.ImageIcon;
import javax.swing.JButton;

public class Button extends JButton {
	private static final long serialVersionUID = 1L;

	protected int x;
	protected int y;
	protected int w;
	protected int h;
	protected int r;

	protected Color borderColor;
	protected Color normalColor;
	protected Color pressedColor;
	protected Color overColor;

	protected String text;
	protected Color textNormalColor;
	protected Color textPressedColor;
	protected Color textOverColor;
	protected int textSize;
	protected String fontName;
	protected boolean hasImage;
	protected ImageIcon image;

	protected boolean mousePressed = false;
	protected boolean mouseOver = false;

	public Button(int x, int y, String text) {
		this.x = x;
		this.y = y;
		this.w = 100;
		this.h = 50;
		this.r = 10;
		this.borderColor = Color.BLACK;
		this.normalColor = new Color(103, 130, 58);
		this.pressedColor = new Color(85, 58, 130);
		this.overColor = Color.WHITE;
		this.text = text;
		this.textNormalColor = Color.WHITE;
		this.textPressedColor = Color.WHITE;
		this.textOverColor = new Color(103, 130, 58);
		this.textSize = h / 3;
		this.fontName = "Dialog";
		this.setPreferredSize(new Dimension(w, h));
		this.setBounds(x, y, w, h);
		this.hasImage = false;
	}

	public Button(int x, int y, String text, boolean reversedColors) {
		this.x = x;
		this.y = y;
		this.w = 100;
		this.h = 50;
		this.r = 10;
		if (reversedColors) {
			this.borderColor = Color.BLACK;
			this.normalColor = Color.WHITE;
			this.pressedColor = new Color(85, 58, 130);
			this.overColor = new Color(103, 130, 58);
			this.text = text;
			this.textNormalColor = new Color(103, 130, 58);
			this.textPressedColor = Color.WHITE;
			this.textOverColor = Color.WHITE;
		} else {
			this.borderColor = Color.BLACK;
			this.normalColor = new Color(103, 130, 58);
			this.pressedColor = new Color(85, 58, 130);
			this.overColor = Color.WHITE;
			this.text = text;
			this.textNormalColor = Color.WHITE;
			this.textPressedColor = Color.WHITE;
			this.textOverColor = new Color(103, 130, 58);
		}
		this.textSize = h / 3;
		this.fontName = "Dialog";
		this.setPreferredSize(new Dimension(w, h));
		this.setBounds(x, y, w, h);
		this.hasImage = false;
	}

	public Button(int x, int y, int w, int h, int r, String text) {
		this.x = x;
		this.y = y;
		this.w = w;
		this.h = h;
		this.r = r;
		this.borderColor = Color.BLACK;
		this.normalColor = new Color(103, 130, 58);
		this.pressedColor = new Color(85, 58, 130);
		this.overColor = Color.WHITE;
		this.text = text;
		this.textNormalColor = Color.WHITE;
		this.textPressedColor = Color.WHITE;
		this.textOverColor = new Color(103, 130, 58);
		this.textSize = h / 3;
		this.fontName = "Dialog";
		this.setPreferredSize(new Dimension(w, h));
		this.setBounds(x, y, w, h);
		this.hasImage = false;
	}

	public Button(int x, int y, int w, int h, int r, String text, boolean reversedColors) {
		this.x = x;
		this.y = y;
		this.w = w;
		this.h = h;
		this.r = r;
		if (reversedColors) {
			this.borderColor = Color.BLACK;
			this.normalColor = Color.WHITE;
			this.pressedColor = new Color(85, 58, 130);
			this.overColor = new Color(103, 130, 58);
			this.text = text;
			this.textNormalColor = new Color(103, 130, 58);
			this.textPressedColor = Color.WHITE;
			this.textOverColor = Color.WHITE;
		} else {
			this.borderColor = Color.BLACK;
			this.normalColor = new Color(103, 130, 58);
			this.pressedColor = new Color(85, 58, 130);
			this.overColor = Color.WHITE;
			this.text = text;
			this.textNormalColor = Color.WHITE;
			this.textPressedColor = Color.WHITE;
			this.textOverColor = new Color(103, 130, 58);
		}
		this.textSize = h / 3;
		this.fontName = "Dialog";
		this.setPreferredSize(new Dimension(w, h));
		this.setBounds(x, y, w, h);
		this.hasImage = false;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getText(String text) {
		return this.text;
	}

	public void setImage(ImageIcon img) {
		this.hasImage = true;
		this.image = img;
	}

	protected void processMouseEvent(MouseEvent e) {
		super.processMouseEvent(e);
		if (e.getID() == MouseEvent.MOUSE_PRESSED) {
			mousePressed = true;
			this.repaint();
		}
		if (e.getID() == MouseEvent.MOUSE_RELEASED) {
			mousePressed = false;
			this.repaint();
		}
		if (e.getID() == MouseEvent.MOUSE_ENTERED) {
			mouseOver = true;
			this.repaint();
		}
		if (e.getID() == MouseEvent.MOUSE_EXITED) {
			mouseOver = false;
			this.repaint();
		}
	}

	@Override
	public void paint(Graphics g) {
		g.clearRect(0, 0, this.getWidth(), this.getHeight());
		paintComponent(g);
		paintBorder(g);
	}

	@Override
	protected void paintComponent(Graphics g) {
		Graphics2D g2D = (Graphics2D) g;
		if (mousePressed) {
			g2D.setColor(this.pressedColor);
		} else {
			if (mouseOver) {
				g2D.setColor(this.overColor);
			} else {
				g2D.setColor(this.normalColor);
			}
		}
		g2D.fillRoundRect(0, 0, this.w - 2, this.h - 2, this.r, this.r);
		if (this.hasImage)
			drawImageCentered(g);
		else
			drawStringCentered(g);
	}

	public void drawStringCentered(Graphics g) {
		g.setFont(new Font(this.fontName, Font.BOLD, textSize));
		Rectangle2D r2D = g.getFont().getStringBounds(this.text, g.getFontMetrics().getFontRenderContext());
		int rW = (int) Math.round(r2D.getWidth());
		int rH = (int) Math.round(r2D.getHeight());
		int rX = (int) Math.round(r2D.getX());
		int rY = (int) Math.round(r2D.getY());
		int a = (this.w / 2) - (rW / 2) - rX;
		int b = (this.h / 2) - (rH / 2) - rY;
		if (mousePressed) {
			g.setColor(this.textPressedColor);
		} else {
			if (mouseOver) {
				g.setColor(this.textOverColor);
			} else {
				g.setColor(this.textNormalColor);
			}
		}
		g.drawString(this.text, a, b);
	}

	public void drawImageCentered(Graphics g) {
		g.drawImage(this.image.getImage(), this.x + this.w / 4, this.y + this.h / 4, this.w / 2, this.h / 2, null);
	}

	@Override
	protected void paintBorder(Graphics g) {
		Graphics2D g2D = (Graphics2D) g;
		g2D.setColor(this.borderColor);
		g2D.drawRoundRect(0, 0, this.w - 2, this.h - 2, this.r, this.r);
	}
}