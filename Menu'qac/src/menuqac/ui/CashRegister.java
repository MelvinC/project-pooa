package menuqac.ui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import menuqac.Menu;
import menuqac.Order;
import menuqac.Prototype;
import menuqac.Table;
import menuqac.ui.button.DeselectButton;
import menuqac.ui.button.InitButton;
import menuqac.ui.button.PaymentButton;
import menuqac.ui.button.RadioButton;
import menuqac.ui.button.SelectButton;
import menuqac.ui.button.SettingsButton;
import menuqac.ui.dropdown.DropdownCashRegister;

public class CashRegister {
	public static int windowWidth = 1920;
	public static int windowHeight = 1080;
	public static int frameWidth = 1500;
	public static int frameHeight = 900;
	public static int WIDTH_BUTTON_PAY = 500;
	public static int HEIGHT_BUTTON_PAY = 50;
	public static final int WIDTH_SCROLL_PANE = 3 * frameWidth / 8;
	public static final DecimalFormat df = new DecimalFormat("####.##");
	private static final int port = 7777;

	private Map<Table, Float> tables;
	private Table activeTable;
	private boolean personnalisable;
	private Map<Socket, Waiter> waiters;
	private Map<Socket, Cooker> cookers;
	private ServerSocket sSocket;
	private Socket socket;

	private JFrame frame;
	private JPanel container;
	private JPanel panelTables;
	private JPanel panelOrders;
	private JPanel leftcenterPanel;
	private JPanel leftPanel;
	private JPanel rightPanel;
	private DropdownCashRegister listTables;
	private JList<Object> listPrototypes;
	private JScrollPane scrollpane;
	private PaymentButton cbButton;
	private PaymentButton chequeButton;
	private PaymentButton especeButton;
	private DeselectButton deselectButton;
	private SelectButton selectButton;
	private RadioButton paymentPersoButton;
	private InitButton initButton;
	private SettingsButton settingsButton;
	private JLabel price;
	private JTextField priceToPay;
	private Menu menu;

	public CashRegister() {
		this.waiters = new HashMap<Socket, Waiter>();
		this.cookers = new HashMap<Socket, Cooker>();
		this.tables = new LinkedHashMap<Table, Float>();
		this.menu = new Menu();
		this.personnalisable = false;
		this.setDefaultTable();
		this.activeTable = (Table) this.tables.keySet().toArray()[0];
		try {
			this.sSocket = new ServerSocket(CashRegister.port);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void setDefaultTable() {
		int nTable = 0;
		try {
			FileReader fr = new FileReader("data/NombreTable.txt");
			BufferedReader br = new BufferedReader(fr);
			nTable = Integer.parseInt(br.readLine());
			br.close();
		} catch (NumberFormatException | IOException e) {
			e.printStackTrace();
		}
		this.tables.put(new Table(1), 0f);
		for (int i = 2; i <= nTable; i++) {
			this.tables.put(new Table(i), 0f);
		}
	}

	public void network() {
		while (true) {
			try {
				this.socket = this.sSocket.accept();
				ObjectInputStream ois = new ObjectInputStream(socket.getInputStream());
				switch (ois.readInt()) {
				case 1:
					Order temp = (Order) ois.readObject();
					this.addOrderToTable(temp);
					for (Iterator<Socket> iter = this.cookers.keySet().iterator(); iter.hasNext();) {
						Socket key = iter.next();
						ObjectOutputStream oos = new ObjectOutputStream(key.getOutputStream());
						oos.writeObject(temp);
					}
					this.repaint();
					break;
				case 2:
					Waiter waiter = (Waiter) ois.readObject();
					for (Iterator<Socket> iter = this.waiters.keySet().iterator(); iter.hasNext();) {
						Socket key = iter.next();
						if (waiters.get(key).getNumber() == waiter.getNumber()) {
							this.sendToWaiter(key);
						}
					}
					break;
				case 3:
					this.cookers.put(this.socket, (Cooker) ois.readObject());
					break;
				case 4:
					this.waiters.put(this.socket, (Waiter) ois.readObject());
					this.sendToWaiter(this.socket);
					break;
				default:
					break;
				}
			} catch (IOException | ClassNotFoundException e) {
				e.printStackTrace();
			}
		}
	}

	public void sendToWaiter(Socket socket) {
		try {
			ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());
			oos.writeObject(this.menu);
			for (Iterator<Table> iter = this.tables.keySet().iterator(); iter.hasNext();) {
				Table key = iter.next();
				key.getNumber();
				oos.writeObject(key);
			}
			oos.writeObject("stop");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void addOrderToTable(Order o) {
		for (Table t : this.tables.keySet()) {
			if (t.getNumber() == o.getTableNumber()) {
				t.addOrder(o);
				this.tables.replace(t, t.getPrice());
			}
		}
	}

	public void addTable(Table t) {
		this.tables.put(t, t.getPrice());
		if (this.listTables != null) {
			this.listTables.addItem(t);
		}
		File nTable = new File("./data", "NombreTable.txt");
		try {
			BufferedWriter bw = new BufferedWriter(new FileWriter(nTable));
			bw.write(((Integer) this.listTables.getItemCount()).toString());
			bw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void deleteTable() {
		if (this.listTables.getItemCount() == 1) {
			return;
		}
		Table t = (Table) this.listTables.getItemAt(this.listTables.getItemCount() - 1);
		this.listTables.removeItem(t);
		this.tables.remove(t);
		File nTable = new File("./data", "NombreTable.txt");
		try {
			BufferedWriter bw = new BufferedWriter(new FileWriter(nTable));
			bw.write(((Integer) this.listTables.getItemCount()).toString());
			bw.flush();
			bw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void removeTable(Table t) {
		this.tables.remove(t);
	}

	public void setActiveTable() {
		this.activeTable = (Table) this.listTables.getSelectedItem();
		this.repaint();
	}

	public void changePrice() {
		try {
			this.tables.replace(this.activeTable,
					this.tables.get(this.activeTable) - (df.parse((this.priceToPay.getText())).floatValue()));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		if (this.tables.get(this.activeTable) <= 0) {
			this.initButton.setVisible(true);
		}
		this.repaint();
	}

	public float getSelectedPrice() {
		float result = 0;
		for (Prototype p : this.activeTable.getPrototypes())
			result += p.isSelected() ? p.getPrice() : 0;
		return result;
	}

	public void selectAll() {
		for (Prototype p : this.activeTable.getPrototypes()) {
			if (!p.isPayed()) {
				p.setSelected(true);
			}
		}
	}

	public void deselectAll() {
		for (Prototype p : this.activeTable.getPrototypes()) {
			p.setSelected(false);
		}
	}

	public void blockPayedPrototypes() {
		for (Prototype p : this.activeTable.getPrototypes()) {
			if (p.isSelected()) {
				p.payed();
				p.setSelected(false);
			}
		}
	}

	public void setPersonnalisable(boolean b) {
		this.personnalisable = b;
		this.priceToPay.setEditable(b);
		if (b) {
			deselectAll();
		} else {
			selectAll();
		}
	}

	public boolean getPersonnalisable() {
		return this.personnalisable;
	}

	public Map<Table, Float> getListTable() {
		return this.tables;
	}

	public void initTable() {
		if (this.tables.get(this.activeTable) <= 0) {
			this.activeTable.flush();
			this.initButton.setVisible(false);
		}
	}

	public void setMenu() {
		this.menu = new Menu();
	}

	public void doGraphics() {
		this.frame = new JFrame("Cash Register");
		this.frame.setSize(frameWidth, frameHeight);
		this.frame.setLocationRelativeTo(null);
		this.frame.setLocation(windowWidth / 2 - frameWidth / 2, windowHeight / 2 - frameHeight / 2);
		this.frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.frame.setBackground(Color.WHITE);

		this.container = new JPanel();
		this.container.setBackground(Color.WHITE);
		this.panelOrders = new JPanel();
		this.panelOrders.setBackground(Color.WHITE);

		this.panelTables = new JPanel();
		ArrayList<Table> array = new ArrayList<Table>();
		array.addAll(this.tables.keySet());
		this.listTables = new DropdownCashRegister(0, 0, array, this);
		this.panelTables.add(listTables);
		this.panelTables.setMaximumSize(new Dimension(this.frame.getWidth(), this.frame.getHeight() / 8));
		this.initButton = new InitButton(0, 0, 200, 50, 10, "Vider", this);
		this.settingsButton = new SettingsButton(0, 0, 50, 50, 10, "settings", this);
		this.settingsButton.setImage(new ImageIcon("data/settings.png"));
		this.panelTables.add(this.initButton);
		this.initButton.setVisible(false);
		this.panelTables.add(this.settingsButton);
		this.panelTables.setBackground(Color.WHITE);

		this.leftPanel = new JPanel();
		this.leftPanel.setLayout(new FlowLayout());
		this.listPrototypes = new JList<Object>();
		this.listPrototypes.setListData((Object[]) this.activeTable.getPrototypes().toArray());
		this.listPrototypes.setCellRenderer(new CustomListRendering());
		this.listPrototypes.addMouseListener(new MouseListListener(this));
		
		//this.scrollpane = new JScrollPane(this.listPrototypes);
		//this.scrollpane.setPreferredSize(new Dimension(WIDTH_SCROLL_PANE, 4 * this.frame.getHeight() / 8));
		
		this.scrollpane=new ScrollPane(this.listPrototypes, 0, 0, WIDTH_SCROLL_PANE, this.frame.getHeight() / 2);
		
		this.leftcenterPanel = new JPanel();
		this.leftcenterPanel.setPreferredSize(new Dimension(this.frame.getWidth() / 2, 5 * this.frame.getHeight() / 8));
		this.leftcenterPanel.add(this.scrollpane);
		this.leftcenterPanel.setBackground(Color.WHITE);
		this.leftPanel.add(this.leftcenterPanel);
		this.selectButton = new SelectButton(0, 0, WIDTH_BUTTON_PAY / 2, HEIGHT_BUTTON_PAY, 10, "SELECT ALL", this);
		this.deselectButton = new DeselectButton(0, 0, WIDTH_BUTTON_PAY / 2, HEIGHT_BUTTON_PAY, 10, "DESELECT ALL",
				this);
		this.leftcenterPanel.add(this.selectButton);
		this.leftcenterPanel.add(this.deselectButton);
		this.leftPanel.setMaximumSize(new Dimension(this.frame.getWidth() / 2, 5 * this.frame.getHeight() / 8));
		this.rightPanel = new JPanel();
		this.rightPanel.setLayout(new FlowLayout());
		this.price = new JLabel(
				"<html><p>Reste � payer : " + df.format(this.tables.get(this.activeTable)) + "�</p></html>");
		this.price.setPreferredSize(new Dimension(WIDTH_BUTTON_PAY, HEIGHT_BUTTON_PAY));
		JLabel payment = new JLabel("<html><p>Paiement :</p></html>");
		payment.setPreferredSize(new Dimension(WIDTH_BUTTON_PAY, HEIGHT_BUTTON_PAY));
		JPanel radio = new JPanel();
		radio.setLayout(new FlowLayout());
		radio.setPreferredSize(new Dimension(WIDTH_BUTTON_PAY, HEIGHT_BUTTON_PAY));
		JLabel perso = new JLabel("<html><p>Mode personnalis�</p></html>");
		perso.setPreferredSize(new Dimension(WIDTH_BUTTON_PAY - 50, HEIGHT_BUTTON_PAY));
		this.paymentPersoButton = new RadioButton(0, 0, 40, 20, 10, "", this);
		radio.add(this.paymentPersoButton);
		radio.add(perso);
		radio.setBackground(Color.WHITE);
		this.priceToPay = new JTextField(df.format(this.getSelectedPrice()));
		this.priceToPay.setHorizontalAlignment(JTextField.CENTER);
		this.priceToPay.setPreferredSize(new Dimension(WIDTH_BUTTON_PAY, HEIGHT_BUTTON_PAY));
		this.priceToPay.setEditable(false);
		this.cbButton = new PaymentButton(0, 0, WIDTH_BUTTON_PAY, HEIGHT_BUTTON_PAY, 10, "CB", this);
		this.chequeButton = new PaymentButton(0, 0, WIDTH_BUTTON_PAY, HEIGHT_BUTTON_PAY, 10, "Ch�que", this);
		this.especeButton = new PaymentButton(0, 0, WIDTH_BUTTON_PAY, HEIGHT_BUTTON_PAY, 10, "Esp�ces", this);
		this.rightPanel.add(this.price);
		this.rightPanel.add(radio);
		this.rightPanel.add(payment);
		this.rightPanel.add(this.priceToPay);
		this.rightPanel.add(this.cbButton);
		this.rightPanel.add(this.chequeButton);
		this.rightPanel.add(this.especeButton);
		this.rightPanel.setPreferredSize(new Dimension(this.frame.getWidth() / 2, 5 * this.frame.getHeight() / 8));
		this.panelOrders.setLayout(new BoxLayout(this.panelOrders, BoxLayout.X_AXIS));
		this.leftPanel.setBackground(Color.WHITE);
		this.rightPanel.setBackground(Color.WHITE);

		this.panelOrders.add(this.leftPanel);
		this.panelOrders.add(this.rightPanel);
		this.panelOrders.setMaximumSize(new Dimension(this.frame.getWidth(), 5 * this.frame.getHeight() / 8));

		BoxLayout fl = new BoxLayout(this.container, BoxLayout.Y_AXIS);
		this.container.setLayout(fl);

		this.container.add(panelTables);
		this.container.add(panelOrders);
		this.frame.add(container);
		this.frame.getContentPane();
		this.frame.setVisible(true);
	}

	public void repaint() {
		this.price.setText("<html><p>Reste � payer : " + df.format(this.tables.get(this.activeTable)) + "�</p></html>");
		this.priceToPay.setText(df.format(this.getSelectedPrice()));
		this.listPrototypes.setListData(this.activeTable.getPrototypes().toArray());
	}

	public static void main(String s[]) {
		CashRegister c = new CashRegister();
		c.doGraphics();
		c.frame.repaint();
		c.network();
	}
}