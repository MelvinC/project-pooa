package menuqac.ui;

import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

public class CashRegisterItemChangeListener implements ItemListener {

	private CashRegister cashRegister;

	public CashRegisterItemChangeListener(CashRegister cashRegister) {
		this.cashRegister = cashRegister;
	}

	@Override
	public void itemStateChanged(ItemEvent e) {
		this.cashRegister.setActiveTable();
	}
}