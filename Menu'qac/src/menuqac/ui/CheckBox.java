package menuqac.ui;

import java.awt.Color;
import javax.swing.JCheckBox;

public class CheckBox extends JCheckBox {
	private static final long serialVersionUID = 1L;

	public CheckBox(int y, int w, boolean isSelected) {
		this.setSelected(isSelected);
		this.setBounds(100 - 4, y + 2, w, 25);
		this.setAlignmentX(LEFT_ALIGNMENT);
		this.setBackground(Color.LIGHT_GRAY);
	}

	public CheckBox(int x, int y, int w, int h, boolean isSelected) {
		this.setSelected(isSelected);
		this.setBounds(x, y, w, h);
		this.setAlignmentX(LEFT_ALIGNMENT);
		this.setBackground(Color.WHITE);
	}
}