package menuqac.ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import menuqac.Menu;
import menuqac.Order;
import menuqac.Prototype;
import menuqac.menu.Section;
import menuqac.ui.button.ParamButton;

public class Cooker implements Serializable {
	private static final long serialVersionUID = 1L;
	private static final int serverPort = 7777;
	private static final String hostname = "localhost";
	private static final int typeMessage = 3;
	public static int windowWidth = 1920;
	public static int windowHeight = 1080;
	public static int frameWidth = 1500;
	public static int frameHeight = 900;
	private transient Socket sock;

	private List<Order> orders;
	private ArrayList<Section> activity;
	private ArrayList<CheckBox> checkBoxes;
	private ArrayList<Prototype> aFaire;
	private ArrayList<Prototype> enCours;

	private Menu menu;

	private JFrame frame;
	private ScrollPane scrollCookAFaire;
	private ScrollPane scrollCookEnCours;
	private PanelToCook panelToCookAFaire;
	private PanelToCook panelToCookEnCours;

	public Cooker() {
		this.orders = new ArrayList<Order>();
		this.activity = new ArrayList<Section>();
		this.checkBoxes = new ArrayList<CheckBox>();
		this.menu = new Menu();
		for (int i = 0; i < menu.getSections().size(); i++) {
			checkBoxes.add(new CheckBox(i, i, false));
		}
		aFaire = new ArrayList<Prototype>();
		this.setAFaire();
		enCours = new ArrayList<Prototype>();
		try {
			this.sock = new Socket(Cooker.hostname, Cooker.serverPort);
			ObjectOutputStream oos = new ObjectOutputStream(sock.getOutputStream());
			oos.writeInt(Cooker.typeMessage);
			oos.writeObject(this);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void repaint() {
		this.frame.getIgnoreRepaint();
		this.scrollCookAFaire.repaint();
		this.scrollCookEnCours.repaint();
	}

	public void setAFaire() {
		ArrayList<String> listActivities = new ArrayList<String>();
		for (Section s : this.activity) {
			if (!listActivities.contains(s.getName())) {
				listActivities.add(s.getName().replaceAll("\\s", ""));
			}
		}
		this.aFaire.clear();
		for (int i = 0; i < orders.size(); i++) {
			for (int j = 0; j < orders.get(i).get().size(); j++) {
				Prototype p = orders.get(i).get().get(j);
				if (listActivities.contains(p.getClass().getSimpleName())) {
					aFaire.add(p);
				}
			}
		}
		if (this.panelToCookAFaire != null) {
			this.scrollCookAFaire.repaint();
			this.scrollCookEnCours.repaint();
		}
	}

	public ArrayList<Prototype> getEnCours() {
		return this.enCours;
	}

	public ArrayList<Prototype> getAFaire() {
		return this.aFaire;
	}

	public ArrayList<CheckBox> getCheckBoxes() {
		return this.checkBoxes;
	}

	public void removePrototype(Prototype p) {
		for (Order o : this.orders) {
			if (o.get().contains(p)) {
				o.get().remove(p);
			}
		}
	}

	public void setActivity(ArrayList<CheckBox> checkBoxes) {
		this.checkBoxes = checkBoxes;
		this.activity = new ArrayList<Section>();
		for (int i = 0; i < menu.getSections().size(); i++) {
			if (checkBoxes.get(i).isSelected()) {
				this.activity.add(this.menu.getSections().get(i));
			}
		}
	}

	public ArrayList<Section> getActivity() {
		return this.activity;
	}

	public void network() {
		while (true) {
			try {
				ObjectInputStream ois = new ObjectInputStream(sock.getInputStream());
				orders.add((Order) ois.readObject());
				this.setAFaire();
				this.frame.repaint();
			} catch (IOException | ClassNotFoundException e) {
				e.printStackTrace();
			}
		}
	}

	public static void main(String s[]) {
		Cooker c = new Cooker();
		c.doGraphics();
		c.network();
	}

	public void doGraphics() {
		this.frame = new JFrame("Cooker");
		this.frame.setSize(frameWidth, frameHeight);
		this.frame.setLocationRelativeTo(null);
		this.frame.setLocation(windowWidth / 2 - frameWidth / 2, windowHeight / 2 - frameHeight / 2);
		this.frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		JPanel panel = new JPanel();
		panel.setBackground(Color.WHITE);
		panel.setAlignmentX(JPanel.LEFT_ALIGNMENT);
		panel.setLayout(null);
		panel.add(new ParamButton(650, 0, 200, 50, 5, "Select Activity", this));

		this.panelToCookAFaire = new PanelToCook(this.aFaire, this, true);
		this.scrollCookAFaire = new ScrollPane(panelToCookAFaire, 100, 75, 650, 750);
		panel.add(this.scrollCookAFaire, BorderLayout.WEST);

		this.panelToCookEnCours = new PanelToCook(this.enCours, this, false);
		this.scrollCookEnCours = new ScrollPane(panelToCookEnCours, 750, 75, 650, 750);
		panel.add(this.scrollCookEnCours, BorderLayout.WEST);

		JLabel label = new JLabel("Pending");
		label.setFont(new Font("Dialog", Font.BOLD, 20));
		label.setBounds(100, 20, 650, 25);
		label.setHorizontalAlignment(SwingConstants.CENTER);
		panel.add(label);

		label = new JLabel("Processing");
		label.setFont(new Font("Dialog", Font.BOLD, 20));
		label.setBounds(750, 20, 650, 25);
		label.setHorizontalAlignment(SwingConstants.CENTER);
		panel.add(label);

		frame.add(panel);
		frame.getContentPane();
		frame.setVisible(true);
	}

	public PanelToCook getPanelToCookAFaire() {
		return this.panelToCookAFaire;
	}

	public PanelToCook getPanelToCookEnCours() {
		return this.panelToCookEnCours;
	}
}