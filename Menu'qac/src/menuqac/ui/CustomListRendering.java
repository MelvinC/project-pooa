package menuqac.ui;

import java.awt.Color;
import java.awt.Component;
import javax.swing.JCheckBox;
import javax.swing.JList;
import javax.swing.ListCellRenderer;
import menuqac.Prototype;

class CustomListRendering extends JCheckBox implements ListCellRenderer<Object> {
	private static final long serialVersionUID = 1L;

	public Component getListCellRendererComponent(JList<?> list, Object value, int index, boolean isSelected,
			boolean hasFocus) {
		setEnabled(list.isEnabled());
		setSelected(((Prototype) value).isSelected());
		setFont(list.getFont());
		setForeground(list.getForeground());
		if (((Prototype) value).isPayed()) {
			setBackground(new Color(144, 238, 144));
			setText("<html><body style=\"text-decoration: line-through; text-indent: 10px; width: "
					+ 2.8 * CashRegister.WIDTH_SCROLL_PANE / 4 + "px;\">" + value.toString());
		} else {
			setBackground(((Prototype) value).isSelected() ? Color.GRAY : list.getBackground());
			setText("<html><body style=\" text-indent: 10px; width: " + 2.8 * CashRegister.WIDTH_SCROLL_PANE / 4
					+ "px;\">" + value.toString());
		}
		return this;
	}
}