package menuqac.ui;

import javax.swing.JScrollBar;

public class CustomScrollBar extends JScrollBar {
	private static final long serialVersionUID = 1L;

	public CustomScrollBar() {
		super();
		this.setUI(new CustomScrollBarUI());
	}
}