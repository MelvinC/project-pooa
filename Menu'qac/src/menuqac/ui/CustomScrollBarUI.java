package menuqac.ui;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.plaf.basic.BasicScrollBarUI;

public class CustomScrollBarUI extends BasicScrollBarUI {
	public CustomScrollBarUI() {
		super();
	}

	@Override
	protected void paintTrack(Graphics g, JComponent c, Rectangle trackBounds) {
		g.clearRect(trackBounds.x, trackBounds.y, trackBounds.width, trackBounds.height);
		g.setColor(new Color(85, 58, 130));
		g.fillRoundRect(trackBounds.x, trackBounds.y, trackBounds.width, trackBounds.height, trackBounds.width,
				trackBounds.width);
		g.setColor(Color.WHITE);
		g.fillRoundRect(trackBounds.x + 2, trackBounds.y + 2, trackBounds.width - 4, trackBounds.height - 4,
				trackBounds.width - 4, trackBounds.width - 4);
	}

	@Override
	protected void paintThumb(Graphics g, JComponent c, Rectangle trackBounds) {
		g.setColor(new Color(103, 130, 58));
		g.fillRoundRect(trackBounds.x + 2, trackBounds.y + 2, trackBounds.width - 4, trackBounds.height - 3,
				trackBounds.width - 4, trackBounds.width - 4);
	}

	@Override
	protected JButton createDecreaseButton(int orientation) {
		return new Button(0, 0, 0, 0, 0, "");
	}

	@Override
	protected JButton createIncreaseButton(int orientation) {
		return new Button(0, 0, 0, 0, 0, "");
	}
}