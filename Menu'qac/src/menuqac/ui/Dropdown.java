package menuqac.ui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.MouseEvent;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import javax.swing.BorderFactory;
import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JComboBox;
import menuqac.Table;
import menuqac.ui.dropdown.DropdownArrow;
import menuqac.ui.dropdown.DropdownItemRenderer;

public class Dropdown extends JComboBox<Table> {
	private static final long serialVersionUID = 1L;

	private DefaultComboBoxModel<Table> model;

	protected int x;
	protected int y;
	protected int w;
	protected int h;

	protected Color borderColor;
	protected Color normalColor;
	protected Color pressedColor;
	protected Color overColor;
	protected int textSize;
	protected String fontName;
	protected Color textNormalColor;
	protected Color textPressedColor;
	protected Color textOverColor;

	protected boolean mousePressed = false;
	protected boolean mouseOver = false;

	public Dropdown(int x, int y, ArrayList<Table> tables) {
		model = new DefaultComboBoxModel<Table>();
		this.setModel(model);
		for (Table item : tables) {
			model.addElement(item);
		}
		this.setEditable(false);
		this.x = x;
		this.y = y;
		this.w = 500 - 133;
		this.h = 25;
		this.borderColor = Color.BLACK;
		this.normalColor = new Color(103, 130, 58);
		this.pressedColor = new Color(85, 58, 130);
		this.overColor = Color.WHITE;
		this.textSize = h / 2;
		this.fontName = "Dialog";
		this.textNormalColor = Color.WHITE;
		this.textPressedColor = Color.WHITE;
		this.textOverColor = new Color(103, 130, 58);
		this.setPreferredSize(new Dimension(w, h));
		this.setBounds(x, y, w, h);
		this.setRenderer(new DropdownItemRenderer(this));
		this.setUI(DropdownArrow.createUI());
		this.setBorder(BorderFactory.createLineBorder(this.borderColor));
		this.setMaximumRowCount(25);
		this.setEditable(false);
	}

	public Dropdown(int x, int y, int w, int h, ArrayList<Table> tables) {
		model = new DefaultComboBoxModel<Table>();
		this.setModel(model);
		for (Table item : tables) {
			model.addElement(item);
		}
		this.setEditable(false);
		this.x = x;
		this.y = y;
		this.w = w;
		this.h = h;
		this.borderColor = Color.BLACK;
		this.normalColor = new Color(103, 130, 58);
		this.pressedColor = new Color(85, 58, 130);
		this.overColor = Color.WHITE;
		this.textSize = h / 3;
		this.fontName = "Dialog";
		this.textNormalColor = Color.WHITE;
		this.textPressedColor = Color.WHITE;
		this.textOverColor = new Color(103, 130, 58);
		this.setPreferredSize(new Dimension(w, h));
		this.setBounds(x, y, w, h);
		this.setRenderer(new DropdownItemRenderer(this));
		this.setUI(DropdownArrow.createUI());
		this.setBorder(BorderFactory.createLineBorder(this.borderColor));
		this.setMaximumRowCount(25);
		this.setEditable(false);
	}

	protected void processMouseEvent(MouseEvent e) {
		super.processMouseEvent(e);
		if (e.getID() == MouseEvent.MOUSE_PRESSED) {
			mousePressed = true;
			this.repaint();
		}
		if (e.getID() == MouseEvent.MOUSE_RELEASED) {
			mousePressed = false;
			this.repaint();
		}
		if (e.getID() == MouseEvent.MOUSE_ENTERED) {
			mouseOver = true;
			this.repaint();
		}
		if (e.getID() == MouseEvent.MOUSE_EXITED) {
			mouseOver = false;
			this.repaint();
		}
	}

	@Override
	public void paint(Graphics g) {
		paintComponent(g);
		paintBorder(g);
	}

	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		if (mousePressed) {
			g.setColor(this.pressedColor);
		} else {
			if (mouseOver) {
				g.setColor(this.overColor);
			} else {
				g.setColor(this.normalColor);
			}
		}
		g.fillRect(0, 0, this.w, this.h);
		g.setColor(Color.WHITE);
		drawStringCentered(g);
		drawImage(g);
	}

	public void drawStringCentered(Graphics g) {
		g.setFont(new Font(this.fontName, Font.BOLD, textSize));
		Rectangle2D r2D = g.getFont().getStringBounds(this.getSelectedItem().toString(),
				g.getFontMetrics().getFontRenderContext());
		int rW = (int) Math.round(r2D.getWidth());
		int rH = (int) Math.round(r2D.getHeight());
		int rX = (int) Math.round(r2D.getX());
		int rY = (int) Math.round(r2D.getY());
		int a = (this.w / 2) - (rW / 2) - rX;
		int b = (this.h / 2) - (rH / 2) - rY;
		if (mousePressed) {
			g.setColor(this.textPressedColor);
		} else {
			if (mouseOver) {
				g.setColor(this.textOverColor);
			} else {
				g.setColor(this.textNormalColor);
			}
		}
		g.drawString(this.getSelectedItem().toString(), a, b);
	}

	public void drawImage(Graphics g) {
		if (mousePressed) {
			g.drawImage(new ImageIcon("data/downArrow.png").getImage(), this.w - h + this.h / 4, 0 + this.h / 4,
					this.h / 2, this.h / 2, null);
		} else {
			if (mouseOver) {
				g.drawImage(new ImageIcon("data/downArrowGreen.png").getImage(), this.w - h + this.h / 4,
						0 + this.h / 4, this.h / 2, this.h / 2, null);
			} else {
				g.drawImage(new ImageIcon("data/downArrow.png").getImage(), this.w - h + this.h / 4, 0 + this.h / 4,
						this.h / 2, this.h / 2, null);
			}
		}
	}
}