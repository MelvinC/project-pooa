package menuqac.ui;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import menuqac.Prototype;
import menuqac.prototype.Food;
import menuqac.prototype.drink.ColdDrinks;
import menuqac.prototype.drink.HotDrinks;
import menuqac.prototype.food.mainDishes.Meats;
import menuqac.ui.button.BigTextButton;
import menuqac.ui.button.HandleButton;
import menuqac.visitor.SauceVisitor.Sauce;

public class Item extends JPanel {
	private static final long serialVersionUID = 1L;

	public Item(Prototype p, int x, int y, PanelOrder po) {
		this.setBackground(Color.LIGHT_GRAY);
		this.setAlignmentX(JPanel.LEFT_ALIGNMENT);
		this.setLayout(null);
		this.setBounds(x, y, 200, 50);
		JLabel label = new JLabel(p.getName().substring(0, p.getName().length() < 15 ? p.getName().length() : 15));
		label.setFont(new Font("Dialog", Font.BOLD, 15));
		label.setBounds(5, 5, 200, 25);
		this.add(label);

		String str = null;
		if (p instanceof Meats) {
			Meats m = (Meats) p;
			str = m.getCooking() + " " + (m.getSauce().equals(Sauce.NONE) ? "" : m.getSauce() + " ") + p.getComment();
		} else if (p instanceof Food) {
			Food f = (Food) p;
			str = (f.getSauce().equals(Sauce.NONE) ? "" : f.getSauce() + " ") + p.getComment();
		} else if (p instanceof ColdDrinks) {
			ColdDrinks cd = (ColdDrinks) p;
			str = (cd.getIceCubes() ? "w icecubes " : "w/o icecubes ") + p.getComment();
		} else if (p instanceof HotDrinks) {
			HotDrinks hd = (HotDrinks) p;
			str = (hd.getSugar() ? "w sugar " : "w/o sugar ") + p.getComment();
		}

		if (str != null) {
			label = new JLabel(str.toLowerCase().substring(0, str.length() < 15 ? str.length() : 15));
			label.setFont(new Font("Dialog", Font.PLAIN, 15));
			label.setBounds(5, 25, 200, 25);
			this.add(label);
		}
		label = new JLabel("" + p.getQuantity());
		label.setFont(new Font("Dialog", Font.PLAIN, 15));
		label.setBounds(150, 3, 20, 25);
		label.setHorizontalAlignment(SwingConstants.CENTER);
		this.add(label);
		BigTextButton button = new BigTextButton(130, 5, 20, 20, 5, "-", 1.25f, label, p, po);
		this.add(button);
		button = new BigTextButton(170, 5, 20, 20, 5, "+", 1.25f, label, p, po);
		this.add(button);
		button = new BigTextButton(130, 25, 60, 20, 5, "Modify", true, p);
		this.add(button);
	}

	public Item(Prototype p, int x, int y, PanelToCook ptc) {
		this.setBackground(Color.LIGHT_GRAY);
		this.setAlignmentX(JPanel.LEFT_ALIGNMENT);
		this.setLayout(null);
		this.setBounds(x, y, 632, 75);
		JLabel label = new JLabel(p.getName());
		label.setFont(new Font("Dialog", Font.BOLD, 15));
		label.setBounds(10, 5, 632, 25);
		this.add(label);

		label = new JLabel("" + p.getComment());
		label.setFont(new Font("Dialog", Font.PLAIN, 15));
		label.setBounds(10, 25, 632, 25);
		this.add(label);

		String str = null;
		if (p instanceof Meats) {
			Meats m = (Meats) p;
			str = m.getCooking() + " " + (m.getSauce().equals(Sauce.NONE) ? "" : m.getSauce() + " ");
		} else if (p instanceof Food) {
			Food f = (Food) p;
			str = (f.getSauce().equals(Sauce.NONE) ? "" : f.getSauce() + " ");
		} else if (p instanceof ColdDrinks) {
			ColdDrinks cd = (ColdDrinks) p;
			str = (cd.getIceCubes() ? "w icecubes" : "w/o icecubes ");
		} else if (p instanceof HotDrinks) {
			HotDrinks hd = (HotDrinks) p;
			str = (hd.getSugar() ? "w sugar" : "w/o sugar ");
		}
		if (str != null) {
			label = new JLabel(str);
			label.setFont(new Font("Dialog", Font.PLAIN, 15));
			label.setBounds(10, 45, 632, 25);
			this.add(label);
		}
		HandleButton button = new HandleButton(500, 20, 100, 35, 5, ptc.getLeft() ? "HANDLE" : "DONE", p, ptc);
		this.add(button);
	}

	@Override
	protected void paintBorder(Graphics g) {
		g.setColor(Color.BLACK);
		g.drawRect(0, -1, this.getWidth() - 4, this.getHeight());
	}

	@Override
	protected void paintComponent(Graphics g) {
		g.setColor(Color.LIGHT_GRAY);
		g.fillRect(0, -1, this.getWidth() - 4, this.getHeight());
	}
}