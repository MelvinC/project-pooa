package menuqac.ui;

import java.awt.Color;
import java.awt.Font;
import java.awt.geom.Rectangle2D;
import javax.swing.JLabel;

public class Label extends JLabel {
	private static final long serialVersionUID = 1L;

	private int y;
	private int w;
	private String s;

	public Label(int y, int w, String s, boolean centered) {
		super(s);
		this.y = y;
		this.w = w;
		this.s = s;
		if (centered) {
			drawStringCentered();
		} else {
			drawStringRightAligned();
		}
	}

	public void drawStringCentered() {
		super.setForeground(Color.BLACK);
		super.setFont(new Font("Dialog", Font.BOLD, 15));
		Rectangle2D r2D = super.getFont().getStringBounds(s,
				super.getFontMetrics(super.getFont()).getFontRenderContext());
		int rW = (int) Math.round(r2D.getWidth());
		int rH = (int) Math.round(r2D.getHeight());
		int rX = (int) Math.round(r2D.getX());
		int a = (this.w / 2) - (rW / 2) - rX;
		super.setBounds(a - 17 / 2, this.y, rW, rH);
	}

	public void drawStringRightAligned() {
		super.setForeground(Color.BLACK);
		super.setFont(new Font("Dialog", Font.BOLD, 15));
		Rectangle2D r2D = super.getFont().getStringBounds(s,
				super.getFontMetrics(super.getFont()).getFontRenderContext());
		int rW = (int) Math.round(r2D.getWidth());
		int rH = (int) Math.round(r2D.getHeight());
		int a = this.w - rW;// / 2) - rX;
		super.setBounds(a - 17 / 2, this.y, rW, rH);
	}
}