package menuqac.ui;

import java.awt.Color;
import java.awt.Dimension;
import javax.swing.JFrame;
import javax.swing.JPanel;
import menuqac.Prototype;
import menuqac.prototype.Food;
import menuqac.prototype.drink.ColdDrinks;
import menuqac.prototype.drink.HotDrinks;
import menuqac.prototype.food.mainDishes.Meats;
import menuqac.ui.button.SaveChangesButton;
import menuqac.ui.dropdown.DropdownCooking;
import menuqac.ui.dropdown.DropdownSauce;
import menuqac.visitor.CookingVisitor.Cooking;
import menuqac.visitor.SauceVisitor.Sauce;

public class ModifyFrame extends JFrame {
	private static final long serialVersionUID = 1L;

	public static int windowWidth = 1920;
	public static int windowHeight = 1080;
	public static int frameWidth = 400;
	public static int frameHeight = 320;

	public ModifyFrame(Prototype p, ScrollPane sp) {
		super("Modify");
		this.setSize(frameWidth, frameHeight);
		this.setLocationRelativeTo(null);
		this.setLocation(windowWidth / 2 - frameWidth / 2, windowHeight / 2 - frameHeight / 2);
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		this.getContentPane();
		this.setVisible(true);
		JPanel panel = new JPanel();
		panel.setBackground(Color.LIGHT_GRAY);
		panel.setBounds(0, 0, frameWidth, frameHeight);
		panel.setPreferredSize(new Dimension(frameWidth, frameHeight));
		panel.setLayout(null);

		Label label = new Label(15, frameWidth, p.getName(), true);
		panel.add(label);

		label = new Label(55, 100, "Comment :", false);
		panel.add(label);
		TextField tf = new TextField(100, 50, p.getComment());
		panel.add(tf);

		CheckBox cb = null;
		if (p instanceof ColdDrinks) {
			ColdDrinks cd = (ColdDrinks) p;
			label = new Label(105, 100, "Ice Cubes :", false);
			panel.add(label);
			cb = new CheckBox(100, 25, cd.getIceCubes());
			panel.add(cb);
		}

		CheckBox cb2 = null;
		if (p instanceof HotDrinks) {
			HotDrinks hd = (HotDrinks) p;
			label = new Label(105, 100, "Sugar :", false);
			panel.add(label);
			cb2 = new CheckBox(100, 25, hd.getSugar());
			panel.add(cb2);
		}

		DropdownSauce dropdownSauce = null;
		if (p instanceof Food) {
			Food f = (Food) p;
			label = new Label(105, 100, "Sauce :", false);
			panel.add(label);
			dropdownSauce = new DropdownSauce(100, 100, Sauce.values(), f.getSauce());
			panel.add(dropdownSauce);
		}

		DropdownCooking dropdownCooking = null;
		if (p instanceof Meats) {
			Meats m = (Meats) p;
			label = new Label(155, 100, "Cooking :", false);
			panel.add(label);
			dropdownCooking = new DropdownCooking(100, 150, Cooking.values(), m.getCooking());
			panel.add(dropdownCooking);
		}

		SaveChangesButton modify = new SaveChangesButton(125, 200, "Save changes", p, tf, this, sp, cb, cb2,
				dropdownSauce, dropdownCooking);
		panel.add(modify);
		this.add(panel);
	}
}