package menuqac.ui;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.JList;
import menuqac.Prototype;

public class MouseListListener implements MouseListener {
	private CashRegister cashRegister;

	public MouseListListener(CashRegister cr) {
		this.cashRegister = cr;
	}

	@Override
	public void mouseClicked(MouseEvent arg0) {
		JList<?> list = (JList<?>) arg0.getSource();
		int index = list.locationToIndex(arg0.getPoint());
		Prototype item = (Prototype) list.getModel().getElementAt(index);
		if (!item.isPayed() && !this.cashRegister.getPersonnalisable()) {
			item.setSelected(!item.isSelected());
			this.cashRegister.repaint();
		}
	}

	@Override
	public void mouseEntered(MouseEvent arg0) {

	}

	@Override
	public void mouseExited(MouseEvent arg0) {

	}

	@Override
	public void mousePressed(MouseEvent arg0) {

	}

	@Override
	public void mouseReleased(MouseEvent arg0) {

	}
}