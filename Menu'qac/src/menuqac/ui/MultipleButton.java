package menuqac.ui;

import java.awt.Component;
import java.util.ArrayList;
import javax.swing.JPanel;
import menuqac.ui.button.ButtonForMultiple;

public class MultipleButton {
	private ArrayList<ButtonForMultiple> buttons;

	public MultipleButton(ArrayList<String> text, ArrayList<Component> comp, JPanel panel) {
		this.buttons = new ArrayList<ButtonForMultiple>();
		for (int i = 0; i < text.size(); i++) {
			ButtonForMultiple button = new ButtonForMultiple(
					50 + i * ((500 - text.size() * 100) / (text.size() - 1) + 100), 75, text.get(i), this, i, comp);
			this.buttons.add(button);
			panel.add(button);
		}
	}

	public void UnselectOthers(int n) {
		for (int i = 0; i < buttons.size(); i++) {
			if (i != n) {
				buttons.get(i).Unselect();
			}
		}
	}
}