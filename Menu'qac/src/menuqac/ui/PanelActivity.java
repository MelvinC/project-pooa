package menuqac.ui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Point;
import java.util.ArrayList;
import javax.swing.JPanel;
import menuqac.Menu;

public class PanelActivity extends JPanel {
	private static final long serialVersionUID = 1L;

	private Menu m;
	private int w;
	private Cooker c;
	private ArrayList<Activity> activities;

	public PanelActivity(Menu m, int w, Cooker c) {
		this.setPreferredSize(new Dimension(250, 575));
		this.setBackground(Color.LIGHT_GRAY);
		this.setBounds(0, 0, 250, 575);
		this.setLayout(null);
		this.m = m;
		this.w = w;
		this.c = c;
		this.Arrange();
	}

	private void Arrange() {
		this.activities = new ArrayList<Activity>();
		int y = 0;
		for (int i = 0; i < m.getSections().size(); i++) {
			Activity activity = new Activity(m.getSections().get(i), w, y, c.getCheckBoxes().get(i).isSelected());
			this.add(activity);
			this.activities.add(activity);
			y += 50;
		}
		if (m.getSections().size() > 0) {
			y += 1;
		}
		Point p = this.getLocation();
		this.setLocation(p);
		this.setPreferredSize(new Dimension(217, y - 1));
	}

	public ArrayList<CheckBox> getCheckBoxes() {
		ArrayList<CheckBox> checkBoxes = new ArrayList<CheckBox>();
		for (int i = 0; i < this.activities.size(); i++) {
			checkBoxes.add(this.activities.get(i).getCheckBox());
		}
		return checkBoxes;
	}

	@Override
	public void paint(Graphics g) {
		g.clearRect(0, 0, this.getWidth(), this.getHeight());
		this.removeAll();
		Arrange();
		super.paint(g);
	}

	@Override
	public void paintComponent(Graphics g) {

	}

	@Override
	public void paintBorder(Graphics g) {

	}
}