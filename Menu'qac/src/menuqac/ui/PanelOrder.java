package menuqac.ui;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Point;
import javax.swing.JPanel;
import menuqac.Order;
import menuqac.ui.button.Line;

public class PanelOrder extends JPanel {
	private static final long serialVersionUID = 1L;

	private Order order;

	public PanelOrder(Order order) {
		this.setPreferredSize(new Dimension(217, 217));
		this.setBounds(0, 0, 217, 217);
		this.setLayout(null);
		this.order = order;
		this.Arrange();
	}

	public Order getOrder() {
		return this.order;
	}

	private void Arrange() {
		int y = 1;
		if (order.getPrototypes().size() > 0) {
			this.add(new Line(197));
		}
		for (int i = 0; i < order.getPrototypes().size(); i++) {
			this.add(new Item(order.getPrototypes().get(i), 0, y, this));
			y += 50;
		}
		if (order.getPrototypes().size() > 0) {
			y += 1;
		}
		Point p = this.getLocation();
		this.setLocation(p);
		this.setPreferredSize(new Dimension(217, y - 1));
	}

	@Override
	public void paint(Graphics g) {
		g.clearRect(0, 0, this.getWidth(), this.getHeight());
		this.removeAll();
		Arrange();
		super.paint(g);
	}

	@Override
	public void paintComponent(Graphics g) {

	}

	@Override
	public void paintBorder(Graphics g) {

	}
}