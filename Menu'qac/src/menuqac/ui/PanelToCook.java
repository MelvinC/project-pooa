package menuqac.ui;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Point;
import java.util.ArrayList;
import javax.swing.JPanel;
import menuqac.Prototype;
import menuqac.ui.button.Line;

public class PanelToCook extends JPanel {
	private static final long serialVersionUID = 1L;

	private ArrayList<Prototype> prototypes;
	private Cooker c;
	private boolean left;

	public PanelToCook(ArrayList<Prototype> prototypes, Cooker c, boolean left) {
		this.setPreferredSize(new Dimension(632, 632));
		this.setBounds(0, 0, 632, 632);
		this.setLayout(null);
		this.prototypes = prototypes;
		this.c = c;
		this.left = left;
		this.Arrange();
	}

	public Cooker getCooker() {
		return this.c;
	}

	public boolean getLeft() {
		return this.left;
	}

	public ArrayList<Prototype> getPrototypes() {
		return this.prototypes;
	}

	private void Arrange() {
		int y = 0;
		if (this.prototypes.size() > 0) {
			this.add(new Line(629));
		}
		for (int i = 0; i < this.prototypes.size(); i++) {
			this.add(new Item(this.prototypes.get(i), 0, y, this));
			y += 75;
		}
		if (this.prototypes.size() > 0) {
			y += 1;
		}
		Point p = this.getLocation();
		this.setLocation(p);
		this.setPreferredSize(new Dimension(632, y - 1));
	}

	@Override
	public void paint(Graphics g) {
		g.clearRect(0, 0, this.getWidth(), this.getHeight());
		this.removeAll();
		Arrange();
		super.paint(g);
	}

	@Override
	public void paintComponent(Graphics g) {

	}

	@Override
	public void paintBorder(Graphics g) {

	}
}