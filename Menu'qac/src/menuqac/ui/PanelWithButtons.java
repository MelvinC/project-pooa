package menuqac.ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.util.List;
import javax.swing.JLabel;
import javax.swing.JPanel;
import menuqac.Order;
import menuqac.menu.Section;
import menuqac.ui.button.MenuButton;

public class PanelWithButtons extends JPanel {
	private static final long serialVersionUID = 1L;

	private Order order;
	private PanelOrder po;
	private List<Section> toDisplay;

	public PanelWithButtons(List<Section> toDisplay, Order order, PanelOrder po) {
		this.toDisplay = toDisplay;
		this.order = order;
		this.po = po;
		this.setBackground(Color.WHITE);
		this.setPreferredSize(new Dimension(300, 800));
		this.setLayout(null);
		this.Arrange();
	}

	public void setToDisplay(List<Section> toDisplay) {
		this.toDisplay = toDisplay;
	}

	private void Arrange() {
		int x = -100;
		int y = 0;
		for (int i = 0; i < toDisplay.size(); i++) {
			JPanel jpanel = new JPanel();
			jpanel.setLayout(new BorderLayout());
			jpanel.setBackground(Color.WHITE);
			JLabel label = new JLabel(toDisplay.get(i).getName());
			label.setFont(new Font("Dialog", Font.PLAIN, 15));
			label.setBounds(10, 10, 10, 10);
			jpanel.setBounds(0, y, 275, 25);
			jpanel.add(label);
			this.add(jpanel);
			y += 35;
			for (int j = 0; j < toDisplay.get(i).getPrototypes().size(); j++) {
				x += 100;
				if (x > 200) {
					x = 0;
					y += 75;
				}
				MenuButton button = new MenuButton(x, y, 75, 50, 5, toDisplay.get(i).getPrototypes().get(j), this.order,
						this.po);
				this.add(button);
			}
			y += 62;
			if (i != toDisplay.size() - 1) {
				JPanel panel = new JPanel();
				panel.setBounds(0, y, 275, 1);
				panel.setBackground(Color.BLACK);
				this.add(panel);
				y += 13;
				x = -100;
			}
		}
		this.setPreferredSize(new Dimension(300, y));
	}

	public void pleasePaint() {
		this.removeAll();
		this.Arrange();
		super.repaint();
	}
}