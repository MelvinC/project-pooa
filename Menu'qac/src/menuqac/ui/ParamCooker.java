package menuqac.ui;

import java.awt.Color;
import java.awt.Dimension;
import javax.swing.JFrame;
import javax.swing.JPanel;
import menuqac.Menu;
import menuqac.ui.button.SaveChangesButton;

public class ParamCooker extends JFrame {
	private static final long serialVersionUID = 1L;

	public static int windowWidth = 1920;
	public static int windowHeight = 1080;
	public static int frameWidth = 290;
	public static int frameHeight = 525;

	public ParamCooker(Cooker c, Menu m) {
		super("Choose your activity");
		this.setSize(frameWidth, frameHeight);
		this.setLocationRelativeTo(null);
		this.setLocation(windowWidth / 2 - frameWidth / 2, windowHeight / 2 - frameHeight / 2);
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		this.getContentPane();
		this.setVisible(true);

		JPanel panel = new JPanel();
		panel.setBounds(0, 0, frameWidth, frameHeight);
		panel.setPreferredSize(new Dimension(frameWidth, frameHeight));
		panel.setLayout(null);
		panel.setBackground(Color.WHITE);

		PanelActivity pa=new PanelActivity(m, frameWidth, c);
		ScrollPane scrollPane = new ScrollPane(pa, 0,0,frameWidth-14,frameHeight-125);
		this.add(scrollPane);

		SaveChangesButton modify = new SaveChangesButton(frameWidth/2-75, frameHeight-110, "Save changes", c, pa, this);
		panel.add(modify);
		this.add(panel);
	}
}