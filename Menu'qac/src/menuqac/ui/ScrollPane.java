package menuqac.ui;

import java.awt.Dimension;
import java.awt.Point;

import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import menuqac.ui.CustomScrollBar;

public class ScrollPane extends JScrollPane {
	private static final long serialVersionUID = 1L;

	private JPanel component;
	private int w;

	public ScrollPane(JPanel component, int x, int y, int w, int h) {
		super(component);
		this.component = component;
		this.setVerticalScrollBar(new CustomScrollBar());
		this.setBounds(x, y, w - 4, h);
		this.w = w;
		this.setPreferredSize(new Dimension(w, h));
		this.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		this.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		this.setBorder(null);
	}
	
	public ScrollPane(JList<?> list, int x, int y, int w, int h) {
		super(list);
		this.setVerticalScrollBar(new CustomScrollBar());
		this.setBounds(x, y, w - 4, h);
		this.w = w;
		this.setPreferredSize(new Dimension(w, h));
		this.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		this.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		this.setBorder(null);
	}

	@Override
	public void repaint() {
		super.repaint();
		if (component instanceof PanelOrder) {
			int _y = 1;
			for (int i = 0; i < ((PanelOrder) component).getOrder().getPrototypes().size(); i++) {
				_y += 50;
			}
			if (((PanelOrder) component).getOrder().getPrototypes().size() > 0) {
				_y += 1;
			}
			Point p = component.getLocation();
			component.setBounds(0, 0, w - 6, _y - 1);
			component.setLocation(p);
		}
		if (component instanceof PanelToCook) {
			int _y = 1;
			for (int i = 0; i < ((PanelToCook) component).getPrototypes().size(); i++) {
				_y += 75;
			}
			if (((PanelToCook) component).getPrototypes().size() > 0) {
				_y += 1;
			}
			Point p = component.getLocation();
			component.setBounds(0, 0, w - 6, _y - 1);
			component.setLocation(p);
		}
	}
}