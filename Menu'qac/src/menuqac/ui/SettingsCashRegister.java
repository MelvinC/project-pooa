package menuqac.ui;

import java.awt.Dimension;
import java.awt.FlowLayout;
import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import menuqac.Table;
import menuqac.ui.button.AddTableButton;
import menuqac.ui.button.DeleteButton;
import menuqac.ui.button.MajMenuButton;

public class SettingsCashRegister {
	public static int windowWidth = 1920;
	public static int windowHeight = 1080;
	public static int frameWidth = 500;
	public static int frameHeight = 700;

	private CashRegister cashRegister;

	private JFrame frame;
	private JPanel container;
	private JPanel top;
	private JPanel tables;
	private JPanel majMenu;
	private JPanel footer;
	private JLabel tablesText;
	private AddTableButton addTableButton;
	private DeleteButton deleteButton;
	private MajMenuButton majMenuButton;

	public SettingsCashRegister(CashRegister cashRegister) {
		this.cashRegister = cashRegister;

		this.frame = new JFrame("Settings Cash Register");
		this.frame.setSize(SettingsCashRegister.frameWidth, SettingsCashRegister.frameHeight);
		this.frame.setLocationRelativeTo(null);
		this.frame.setLocation(windowWidth / 2 - frameWidth / 2, windowHeight / 2 - frameHeight / 2);
		this.frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

		this.container = new JPanel();
		this.top = new JPanel();
		this.top.setMaximumSize(new Dimension(SettingsCashRegister.frameWidth, SettingsCashRegister.frameHeight));
		this.tables = new JPanel();
		this.tables.setMaximumSize(new Dimension(SettingsCashRegister.frameWidth, SettingsCashRegister.frameHeight));
		this.majMenu = new JPanel();
		this.footer = new JPanel();

		BoxLayout fl = new BoxLayout(this.container, BoxLayout.Y_AXIS);
		this.container.setLayout(fl);

		this.tablesText = new JLabel();
		this.tablesText.setText("<html><a style=\"text-align: right\">Actuellement : "
				+ this.cashRegister.getListTable().size() + " tables</a></html>");
		this.addTableButton = new AddTableButton(0, 0, 50, 50, 10, "+", this);
		this.deleteButton = new DeleteButton(0, 0, 50, 50, 10, "-", this);
		this.tables.setLayout(new BoxLayout(this.tables, BoxLayout.Y_AXIS));
		JPanel buttons = new JPanel();
		buttons.setLayout(new FlowLayout());
		this.tables.add(this.tablesText);
		buttons.add(this.addTableButton);
		buttons.add(this.deleteButton);
		this.tables.add(buttons);

		this.majMenuButton = new MajMenuButton(0, 0, 300, 50, 10, "Update Menu", this.cashRegister);
		this.majMenu.add(this.majMenuButton);
		this.majMenu.setLayout(new FlowLayout());

		this.container.add(this.top);
		this.container.add(this.tables);
		this.container.add(this.majMenu);
		this.container.add(this.footer);

		this.frame.add(this.container);
		this.frame.getContentPane();
		this.frame.setVisible(true);
	}

	public void addTable() {
		this.cashRegister.addTable(new Table(this.cashRegister.getListTable().size() + 1));
	}

	public void deleteTable() {
		this.cashRegister.deleteTable();
	}

	public void repaint() {
		this.tablesText.setText("<html>Actuellement : " + this.cashRegister.getListTable().size() + " tables</html>");
		this.frame.repaint();
		this.cashRegister.repaint();
	}
}