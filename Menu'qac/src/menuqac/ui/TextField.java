package menuqac.ui;

import java.awt.Color;
import java.awt.Font;
import javax.swing.JTextField;

public class TextField extends JTextField {
	private static final long serialVersionUID = 1L;

	public TextField(int x, int y, String s) {
		super(s);
		super.setBounds(x, y, 250, 25);
		super.setAlignmentX(CENTER_ALIGNMENT);
		super.setForeground(new Color(103, 130, 58));
		super.setFont(new Font("Dialog", Font.BOLD, 13));
	}
}