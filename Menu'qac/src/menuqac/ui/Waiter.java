package menuqac.ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.net.Socket;
import java.util.ArrayList;
import javax.swing.JFrame;
import javax.swing.JPanel;
import menuqac.Menu;
import menuqac.Order;
import menuqac.Table;
import menuqac.ui.button.MajButton;
import menuqac.ui.button.SendButton;
import menuqac.ui.dropdown.DropdownWaiter;

public class Waiter implements Serializable {
	private static final long serialVersionUID = 1L;

	private static int windowWidth = 1920;
	private static int windowHeight = 1080;
	private static int frameWidth = 600;
	private static int frameHeight = 750;
	private Order order;

	private static final String hostname = "localhost";
	private static final int port = 7777;
	private static final int typeMessage = 4;
	private int number;
	private transient Socket sock;
	private Menu menu;

	private ArrayList<Table> tables;

	private transient JFrame frame;
	private transient JPanel panel;
	private transient DropdownWaiter dropdown;
	private transient MajButton majButton;
	private transient PanelOrder panelOrder;
	private transient ScrollPane orders;
	private transient ScrollPane starters;
	private transient ScrollPane mainDishes;
	private transient ScrollPane deserts;
	private transient ScrollPane drinks;
	private transient SendButton send;

	private PanelWithButtons pwdStarter;
	private PanelWithButtons pwdMainDishes;
	private PanelWithButtons pwdDeserts;
	private PanelWithButtons pwdDrinks;

	public Waiter(int number) {
		this.setNumber(number);
		this.tables = new ArrayList<Table>();
		try {
			this.setSock(new Socket(Waiter.hostname, Waiter.port));
			ObjectOutputStream oos = new ObjectOutputStream(getSock().getOutputStream());
			oos.writeInt(Waiter.typeMessage);
			oos.writeObject(this);
			ObjectInputStream ois = new ObjectInputStream(getSock().getInputStream());
			this.menu = (Menu) ois.readObject();
			Object temp = ois.readObject();
			while (!temp.equals("stop")) {
				this.getTables().add((Table) temp);
				temp = ois.readObject();
			}
		} catch (IOException | ClassNotFoundException e) {
			e.printStackTrace();
		}
		this.order = new Order(1, number, 1);
	}

	public int getPort() {
		return Waiter.port;
	}

	public void doGraphics() {
		this.frame = new JFrame("Waiter");
		this.panel = new JPanel();
		this.panel.setBackground(Color.WHITE);
		this.panel.setLayout(null);

		this.dropdown = new DropdownWaiter(50, 25, this.tables, this.order);
		this.panel.add(this.dropdown);

		this.majButton = new MajButton(450, 25, "Update", this);
		this.panel.add(this.majButton);

		this.panelOrder = new PanelOrder(this.order);
		this.orders = new ScrollPane(panelOrder, 350, 150, 218, 451);
		this.frame.add(orders, BorderLayout.CENTER);
		this.send = new SendButton(400, 625, "SEND", this.order, panelOrder);
		this.frame.add(send);

		this.pwdStarter = new PanelWithButtons(this.menu.getChildFromParent("Starters"), this.order, this.panelOrder);
		this.starters = new ScrollPane(pwdStarter, 50, 150, 300, 525);
		this.frame.add(starters, BorderLayout.CENTER);

		this.pwdMainDishes = new PanelWithButtons(this.menu.getChildFromParent("Main Dishes"), this.order,
				this.panelOrder);
		this.mainDishes = new ScrollPane(pwdMainDishes, 50, 150, 300, 525);
		this.frame.add(mainDishes, BorderLayout.CENTER);

		this.pwdDeserts = new PanelWithButtons(this.menu.getChildFromParent("Deserts"), this.order, this.panelOrder);
		this.deserts = new ScrollPane(pwdDeserts, 50, 150, 300, 525);
		this.frame.add(deserts, BorderLayout.CENTER);

		this.pwdDrinks = new PanelWithButtons(this.menu.getChildFromParent("Drinks"), this.order, this.panelOrder);
		this.drinks = new ScrollPane(pwdDrinks, 50, 150, 300, 525);
		this.frame.add(drinks, BorderLayout.CENTER);

		ArrayList<String> str = new ArrayList<String>();
		str.add("Starters");
		str.add("Main dishes");
		str.add("Deserts");
		str.add("Drinks");
		ArrayList<Component> com = new ArrayList<Component>();
		com.add(starters);
		com.add(mainDishes);
		com.add(deserts);
		com.add(drinks);
		new MultipleButton(str, com, this.panel);

		this.frame.add(this.panel);
		this.frame.getContentPane();
		this.frame.setSize(Waiter.frameWidth, Waiter.frameHeight);
		this.frame.setLocationRelativeTo(null);
		this.frame.setLocation(Waiter.windowWidth / 2 - Waiter.frameWidth / 2,
				Waiter.windowHeight / 2 - Waiter.frameHeight / 2);
		this.frame.setVisible(true);
		this.frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

	public void repaint() {
		int i = this.dropdown.getItemCount();
		for (Table t : tables) {
			this.dropdown.addItem(t);
		}
		for (int j = 0; j < i; j++) {
			this.dropdown.removeItemAt(0);
		}
		this.pwdStarter.setToDisplay(this.menu.getChildFromParent("Starters"));
		this.pwdStarter.pleasePaint();
		this.pwdMainDishes.setToDisplay(this.menu.getChildFromParent("Main Dishes"));
		this.pwdMainDishes.pleasePaint();
		this.pwdDeserts.setToDisplay(this.menu.getChildFromParent("Deserts"));
		this.pwdDeserts.pleasePaint();
		this.pwdDrinks.setToDisplay(this.menu.getChildFromParent("Drinks"));
		this.pwdDrinks.pleasePaint();
		this.frame.repaint();
	}

	public static void main(String s[]) {
		Waiter waiter = new Waiter(1);
		waiter.doGraphics();
	}

	public int getNumber() {
		return number;
	}

	public void setNumber(int number) {
		this.number = number;
	}

	public Socket getSock() {
		return sock;
	}

	public void setSock(Socket sock) {
		this.sock = sock;
	}

	public Menu getMenu() {
		return menu;
	}

	public void setMenu(Menu menu) {
		this.menu = menu;
	}

	public void emptyTables() {
		this.getTables().clear();
	}

	public void addTables(Table table) {
		this.getTables().add(table);
	}

	public ArrayList<Table> getTables() {
		return tables;
	}
}