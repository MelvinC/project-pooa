package menuqac.ui;

import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import menuqac.Order;
import menuqac.Table;
import menuqac.ui.dropdown.DropdownWaiter;

public class WaiterItemChangeListener implements ItemListener {
	private Order order;
	private DropdownWaiter dropdown;

	public WaiterItemChangeListener(Order order, DropdownWaiter dropdown) {
		this.order = order;
		this.dropdown = dropdown;
	}

	@Override
	public void itemStateChanged(ItemEvent e) {
		this.order.setTableNumber(((Table) this.dropdown.getSelectedItem()).getNumber());
	}
}