package menuqac.ui.button;

import java.awt.event.MouseEvent;
import menuqac.ui.Button;
import menuqac.ui.SettingsCashRegister;

public class AddTableButton extends Button {
	private static final long serialVersionUID = 1L;

	private SettingsCashRegister settingCashRegister;

	public AddTableButton(int x, int y, String text, SettingsCashRegister settingCashRegister) {
		super(x, y, text);
		this.settingCashRegister = settingCashRegister;
	}

	public AddTableButton(int x, int y, int w, int h, int r, String text, SettingsCashRegister settingCashRegister) {
		super(x, y, w, h, r, text);
		this.settingCashRegister = settingCashRegister;
	}

	@Override
	protected void processMouseEvent(MouseEvent e) {
		if (e.getID() == MouseEvent.MOUSE_PRESSED) {
			mousePressed = true;
			this.repaint();
		}
		if (e.getID() == MouseEvent.MOUSE_RELEASED) {
			mousePressed = false;
			if (mouseOver) {
				this.settingCashRegister.addTable();
				this.settingCashRegister.repaint();
			}
			this.repaint();
		}
		if (e.getID() == MouseEvent.MOUSE_ENTERED) {
			mouseOver = true;
			this.repaint();
		}
		if (e.getID() == MouseEvent.MOUSE_EXITED) {
			mouseOver = false;
			this.repaint();
		}
	}
}