package menuqac.ui.button;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.MouseEvent;
import javax.swing.JLabel;
import menuqac.Prototype;
import menuqac.ui.Button;
import menuqac.ui.ModifyFrame;
import menuqac.ui.PanelOrder;
import menuqac.ui.ScrollPane;

public class BigTextButton extends Button {
	private static final long serialVersionUID = 1L;

	private JLabel label;
	private Prototype p;
	private PanelOrder po;

	public BigTextButton(int x, int y, int w, int h, int r, String text, Prototype p, PanelOrder po) {
		super(x, y, w, h, r, text);
		this.textSize = this.h / 2;
		this.p = p;
		this.po = po;
	}

	public BigTextButton(int x, int y, int w, int h, int r, String text, float n, Prototype p, PanelOrder po) {
		super(x, y, w, h, r, text);
		this.textSize = (int) (this.h / n);
		this.p = p;
		this.po = po;
	}

	public BigTextButton(int x, int y, int w, int h, int r, String text, float n, JLabel label, Prototype p,
			PanelOrder po) {
		super(x, y, w, h, r, text);
		this.textSize = (int) (this.h / n);
		this.label = label;
		this.p = p;
		this.po = po;
	}

	public BigTextButton(int x, int y, int w, int h, int r, String text, boolean b, Prototype p) {
		super(x, y, w, h, r, text, b);
		this.textSize = this.h / 2;
		this.p = p;
	}

	protected void processMouseEvent(MouseEvent e) {
		super.processMouseEvent(e);
		if (e.getID() == MouseEvent.MOUSE_PRESSED) {
			mousePressed = true;
			this.repaint();
		}
		if (e.getID() == MouseEvent.MOUSE_RELEASED) {
			mousePressed = false;
			if (mouseOver) {
				if (this.po == null) {
					new ModifyFrame(this.p, (ScrollPane) this.getParent().getParent().getParent().getParent());
				} else {
					if (this.text == "+") {
						this.p.plus1();
					} else if (this.text == "-") {
						this.p.minus1();
					}
					this.label.setText("" + this.p.getQuantity());
					this.po.repaint();
				}
				this.getParent().getParent().getParent()
						.setPreferredSize(this.getParent().getParent().getPreferredSize());
				this.getParent().getParent().getParent().getParent().repaint();
			}
			this.repaint();
		}
		if (e.getID() == MouseEvent.MOUSE_ENTERED) {
			mouseOver = true;
			this.repaint();
		}
		if (e.getID() == MouseEvent.MOUSE_EXITED) {
			mouseOver = false;
			this.repaint();
		}
	}

	@Override
	public void paint(Graphics g) {
		g.setColor(Color.LIGHT_GRAY);
		g.fillRect(0, 0, this.getWidth(), this.getHeight());
		paintComponent(g);
		paintBorder(g);
	}
}