package menuqac.ui.button;

import java.awt.Component;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import menuqac.ui.Button;
import menuqac.ui.MultipleButton;

public class ButtonForMultiple extends Button {
	private static final long serialVersionUID = 1L;

	private boolean selected;
	private int num;
	private MultipleButton mb;

	public ButtonForMultiple(int x, int y, String text, MultipleButton mb, int num, ArrayList<Component> comp) {
		super(x, y, text);
		this.selected = num == 0;
		this.mb = mb;
		this.num = num;
		this.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				comp.get(0).setVisible(num == 0);
				comp.get(1).setVisible(num == 1);
				comp.get(2).setVisible(num == 2);
				comp.get(3).setVisible(num == 3);
			}
		});
	}

	public void Unselect() {
		this.selected = false;
	}

	protected void processMouseEvent(MouseEvent e) {
		super.processMouseEvent(e);
		if (e.getID() == MouseEvent.MOUSE_PRESSED) {
			mousePressed = true;
			this.repaint();
		}
		if (e.getID() == MouseEvent.MOUSE_RELEASED) {
			mousePressed = false;
			if (mouseOver) {
				selected = true;
				mb.UnselectOthers(this.num);
			}
			this.repaint();
		}
		if (e.getID() == MouseEvent.MOUSE_ENTERED) {
			mouseOver = true;
			this.repaint();
		}
		if (e.getID() == MouseEvent.MOUSE_EXITED) {
			mouseOver = false;
			this.repaint();
		}
	}

	@Override
	protected void paintComponent(Graphics g) {
		Graphics2D g2D = (Graphics2D) g;
		if (mousePressed) {
			g2D.setColor(this.pressedColor);
		} else {
			if (mouseOver) {
				g2D.setColor(this.overColor);
			} else {
				g2D.setColor(this.normalColor);
			}
		}
		if (this.selected) {
			g2D.setColor(this.pressedColor);
		}
		g2D.fillRoundRect(0, 0, this.w - 2, this.h - 2, this.r, this.r);
		drawStringCentered(g);
	}

	public void drawStringCentered(Graphics g) {
		g.setFont(new Font(this.fontName, Font.BOLD, textSize));
		Rectangle2D r2D = g.getFont().getStringBounds(this.text, g.getFontMetrics().getFontRenderContext());
		int rW = (int) Math.round(r2D.getWidth());
		int rH = (int) Math.round(r2D.getHeight());
		int rX = (int) Math.round(r2D.getX());
		int rY = (int) Math.round(r2D.getY());
		int a = (this.w / 2) - (rW / 2) - rX;
		int b = (this.h / 2) - (rH / 2) - rY;
		if (mousePressed) {
			g.setColor(this.textPressedColor);
		} else {
			if (mouseOver) {
				g.setColor(this.textOverColor);
			} else {
				g.setColor(this.textNormalColor);
			}
		}
		if (this.selected) {
			g.setColor(this.textPressedColor);
		}
		g.drawString(this.text, a, b);
	}
}