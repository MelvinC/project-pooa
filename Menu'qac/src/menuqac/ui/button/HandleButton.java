package menuqac.ui.button;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.MouseEvent;
import menuqac.Prototype;
import menuqac.ui.Button;
import menuqac.ui.PanelToCook;

public class HandleButton extends Button {
	private static final long serialVersionUID = 1L;

	private Prototype p;
	private PanelToCook ptc;

	public HandleButton(int x, int y, int w, int h, int r, String text, Prototype p, PanelToCook ptc) {
		super(x, y, w, h, r, text);
		this.textSize = this.h / 2;
		this.p = p;
		this.ptc = ptc;
	}

	protected void processMouseEvent(MouseEvent e) {
		super.processMouseEvent(e);
		if (e.getID() == MouseEvent.MOUSE_PRESSED) {
			mousePressed = true;
			this.repaint();
		}
		if (e.getID() == MouseEvent.MOUSE_RELEASED) {
			mousePressed = false;
			if (mouseOver) {
				if (ptc.getLeft()) {
					ptc.getCooker().getAFaire().remove(p);
					ptc.getCooker().removePrototype(p);
					ptc.getCooker().getEnCours().add(p);
				} else {
					ptc.getCooker().getEnCours().remove(p);
				}
				this.ptc.getCooker().repaint();
			}
			this.repaint();
		}
		if (e.getID() == MouseEvent.MOUSE_ENTERED) {
			mouseOver = true;
			this.repaint();
		}
		if (e.getID() == MouseEvent.MOUSE_EXITED) {
			mouseOver = false;
			this.repaint();
		}
	}

	@Override
	public void paint(Graphics g) {
		g.setColor(Color.LIGHT_GRAY);
		g.fillRect(0, 0, this.getWidth(), this.getHeight());
		paintComponent(g);
		paintBorder(g);
	}
}