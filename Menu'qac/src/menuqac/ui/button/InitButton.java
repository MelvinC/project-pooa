package menuqac.ui.button;

import java.awt.event.MouseEvent;
import menuqac.ui.Button;
import menuqac.ui.CashRegister;

public class InitButton extends Button {
	private static final long serialVersionUID = 1L;

	private CashRegister cashRegister;

	public InitButton(int x, int y, String text, CashRegister cashRegister) {
		super(x, y, text);
		this.cashRegister = cashRegister;
	}

	public InitButton(int x, int y, int w, int h, int r, String text, CashRegister cashRegister) {
		super(x, y, w, h, r, text);
		this.cashRegister = cashRegister;
	}

	@Override
	protected void processMouseEvent(MouseEvent e) {
		if (e.getID() == MouseEvent.MOUSE_PRESSED) {
			mousePressed = true;
			this.repaint();
		}
		if (e.getID() == MouseEvent.MOUSE_RELEASED) {
			mousePressed = false;
			if (mouseOver) {
				this.cashRegister.initTable();
				this.cashRegister.repaint();
			}
			this.repaint();
		}
		if (e.getID() == MouseEvent.MOUSE_ENTERED) {
			mouseOver = true;
			this.repaint();
		}
		if (e.getID() == MouseEvent.MOUSE_EXITED) {
			mouseOver = false;
			this.repaint();
		}
	}
}