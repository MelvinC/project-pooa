package menuqac.ui.button;

import java.awt.Graphics;
import menuqac.ui.Button;

public class Line extends Button {
	private static final long serialVersionUID = 1L;

	private int w;

	public Line(int width) {
		super(0, 0, width, 1, 0, "");
		this.w = width;
	}

	@Override
	public void paint(Graphics g) {
		g.fillRect(0, 0, this.w, 1);
	}
}