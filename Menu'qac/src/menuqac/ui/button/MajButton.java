package menuqac.ui.button;

import java.awt.event.MouseEvent;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import menuqac.Menu;
import menuqac.Table;
import menuqac.ui.Button;
import menuqac.ui.Waiter;

public class MajButton extends Button {
	private static final long serialVersionUID = 1L;
	private static final String hostname = "localhost";
	private static final int port = 7777;
	private static final int typeMessage = 2;
	private Waiter waiter;

	public MajButton(int x, int y, String text, Waiter waiter) {
		super(x, y, text, true);
		this.h = 25;
		this.waiter = waiter;
	}

	protected void processMouseEvent(MouseEvent e) {
		super.processMouseEvent(e);
		if (e.getID() == MouseEvent.MOUSE_PRESSED) {
			mousePressed = true;
			this.repaint();
		}
		if (e.getID() == MouseEvent.MOUSE_RELEASED) {
			mousePressed = false;
			if (mouseOver) {
				this.askMaj();
			}
			this.repaint();
		}
		if (e.getID() == MouseEvent.MOUSE_ENTERED) {
			mouseOver = true;
			this.repaint();
		}
		if (e.getID() == MouseEvent.MOUSE_EXITED) {
			mouseOver = false;
			this.repaint();
		}
	}

	private void askMaj() {
		try {
			Socket sock = new Socket(MajButton.hostname, MajButton.port);
			ObjectOutputStream oos = new ObjectOutputStream(sock.getOutputStream());
			oos.writeInt(MajButton.typeMessage);
			oos.writeObject(waiter);
			sock.close();
			ObjectInputStream ois = new ObjectInputStream(this.waiter.getSock().getInputStream());
			this.waiter.setMenu((Menu) ois.readObject());
			Object temp = ois.readObject();
			this.waiter.emptyTables();
			while (!temp.equals("stop")) {
				this.waiter.addTables((Table) temp);
				temp = ois.readObject();
			}
			this.waiter.repaint();
		} catch (IOException | ClassNotFoundException e) {
			e.printStackTrace();
		}
	}
}