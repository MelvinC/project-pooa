package menuqac.ui.button;

import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.MouseEvent;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import menuqac.Order;
import menuqac.Prototype;
import menuqac.ui.Button;
import menuqac.ui.PanelOrder;

public class MenuButton extends Button {
	private static final long serialVersionUID = 1L;

	private Order order;
	private Prototype prototype;
	private PanelOrder po;

	public MenuButton(int x, int y, int w, int h, int r, Prototype prototype, Order order, PanelOrder po) {
		super(x, y, w, h, r, prototype.getName(), true);
		this.order = order;
		this.prototype = prototype;
		this.po = po;
		this.textSize = h / 4;
	}

	@Override
	protected void paintComponent(Graphics g) {
		Graphics2D g2D = (Graphics2D) g;
		if (mousePressed) {
			g2D.setColor(this.pressedColor);
		} else {
			if (mouseOver) {
				g2D.setColor(this.overColor);
			} else {
				g2D.setColor(this.normalColor);
			}
		}
		g2D.fillRoundRect(0, 0, this.w - 2, this.h - 2, this.r, this.r);
		drawStringCentered(g);
	}

	public void drawStringCentered(Graphics g) {
		if (this.text.contains(" ")) {
			ArrayList<String> string = new ArrayList<String>();
			String str = this.text.substring(0, this.text.indexOf(" "));
			string.add(str);
			String rest = this.text.substring(str.length() + 1, this.text.length());
			while (rest.contains(" ")) {
				str = rest.substring(0, rest.indexOf(" "));
				string.add(str);
				rest = rest.substring(str.length() + 1, rest.length());
			}
			string.add(rest);
			for (int i = 0; i < string.size(); i++) {
				g.setFont(new Font(this.fontName, Font.BOLD, textSize));
				Rectangle2D r2D = g.getFont().getStringBounds(string.get(i), g.getFontMetrics().getFontRenderContext());
				int rW = (int) Math.round(r2D.getWidth());
				int rH = (int) Math.round(r2D.getHeight());
				int rX = (int) Math.round(r2D.getX());
				int rY = (int) Math.round(r2D.getY());
				int a = (this.w / 2) - (rW / 2) - rX;
				int b = (this.h / 2) - (rH / 2) - rY;
				if (mousePressed) {
					g.setColor(this.textPressedColor);
				} else {
					if (mouseOver) {
						g.setColor(this.textOverColor);
					} else {
						g.setColor(this.textNormalColor);
					}
				}
				g.drawString(string.get(i), a,
						(int) (b - (this.h * (float) (string.size() - 1) / (float) ((string.size() + 1) * 2))
								+ i * (float) (this.h / (string.size() + 1))));
			}
		} else {
			g.setFont(new Font(this.fontName, Font.BOLD, textSize));
			Rectangle2D r2D = g.getFont().getStringBounds(this.text, g.getFontMetrics().getFontRenderContext());
			int rW = (int) Math.round(r2D.getWidth());
			int rH = (int) Math.round(r2D.getHeight());
			int rX = (int) Math.round(r2D.getX());
			int rY = (int) Math.round(r2D.getY());
			int a = (this.w / 2) - (rW / 2) - rX;
			int b = (this.h / 2) - (rH / 2) - rY;
			if (mousePressed) {
				g.setColor(this.textPressedColor);
			} else {
				if (mouseOver) {
					g.setColor(this.textOverColor);
				} else {
					g.setColor(this.textNormalColor);
				}
			}
			g.drawString(this.text, a, b);
		}
	}

	protected void processMouseEvent(MouseEvent e) {
		super.processMouseEvent(e);
		if (e.getID() == MouseEvent.MOUSE_PRESSED) {
			mousePressed = true;
			this.repaint();
		}
		if (e.getID() == MouseEvent.MOUSE_RELEASED) {
			mousePressed = false;
			if (mouseOver) {
				order.addPrototype(this.prototype.clone());
				this.po.getParent().setPreferredSize(this.po.getPreferredSize());
				this.po.getParent().getParent().repaint();
			}
			this.repaint();
		}
		if (e.getID() == MouseEvent.MOUSE_ENTERED) {
			mouseOver = true;
			this.repaint();
		}
		if (e.getID() == MouseEvent.MOUSE_EXITED) {
			mouseOver = false;
			this.repaint();
		}
	}
}