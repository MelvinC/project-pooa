package menuqac.ui.button;

import java.awt.event.MouseEvent;
import menuqac.Menu;
import menuqac.ui.Button;
import menuqac.ui.Cooker;
import menuqac.ui.ParamCooker;

public class ParamButton extends Button {
	private static final long serialVersionUID = 1L;

	private Cooker cooker;

	public ParamButton(int x, int y, int w, int h, int r, String text, Cooker cooker) {
		super(x, y, w, h, r, text);
		this.cooker = cooker;
	}

	public ParamButton(int x, int y, String text, Cooker cooker) {
		super(x, y, text);
		this.cooker = cooker;
	}

	@Override
	protected void processMouseEvent(MouseEvent e) {
		if (e.getID() == MouseEvent.MOUSE_PRESSED) {
			mousePressed = true;
			this.repaint();
		}
		if (e.getID() == MouseEvent.MOUSE_RELEASED) {
			mousePressed = false;
			if (mouseOver) {
				ParamCooker pc = new ParamCooker(this.cooker, new Menu());
				pc.setVisible(true);
			}
			this.repaint();
		}
		if (e.getID() == MouseEvent.MOUSE_ENTERED) {
			mouseOver = true;
			this.repaint();
		}
		if (e.getID() == MouseEvent.MOUSE_EXITED) {
			mouseOver = false;
			this.repaint();
		}
	}
}