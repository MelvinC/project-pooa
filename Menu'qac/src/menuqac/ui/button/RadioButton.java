package menuqac.ui.button;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.MouseEvent;
import menuqac.ui.Button;
import menuqac.ui.CashRegister;

public class RadioButton extends Button {
	private static final long serialVersionUID = 1L;

	private CashRegister cashRegister;

	public RadioButton(int x, int y, int w, int h, int r, String text, CashRegister cashRegister) {
		super(x, y, w, h, r, text);
		this.cashRegister = cashRegister;
	}

	public RadioButton(int x, int y, String text, CashRegister cashRegister) {
		super(x, y, text);
		this.cashRegister = cashRegister;
	}

	protected void processMouseEvent(MouseEvent e) {
		if (e.getID() == MouseEvent.MOUSE_PRESSED) {
			mousePressed = true;
			this.repaint();
		}
		if (e.getID() == MouseEvent.MOUSE_RELEASED) {
			mousePressed = false;
			if (mouseOver) {
				this.cashRegister.setPersonnalisable(!this.cashRegister.getPersonnalisable());
				this.cashRegister.repaint();
			}
			this.repaint();
		}
		if (e.getID() == MouseEvent.MOUSE_ENTERED) {
			mouseOver = true;
			this.repaint();
		}
		if (e.getID() == MouseEvent.MOUSE_EXITED) {
			mouseOver = false;
			this.repaint();
		}
	}

	@Override
	protected void paintComponent(Graphics g) {
		Graphics2D g2D = (Graphics2D) g;
		g2D.setColor(this.cashRegister.getPersonnalisable() ? new Color(144, 238, 144) : new Color(240, 128, 128));
		g2D.fillRoundRect(0, 0, this.w - 2, this.h - 2, this.r, this.r);
		drawStringCentered(g);
	}
}