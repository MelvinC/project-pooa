package menuqac.ui.button;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.MouseEvent;
import java.awt.event.WindowEvent;
import menuqac.Prototype;
import menuqac.ui.Button;
import menuqac.ui.CheckBox;
import menuqac.ui.Cooker;
import menuqac.ui.ModifyFrame;
import menuqac.ui.PanelActivity;
import menuqac.ui.ParamCooker;
import menuqac.ui.ScrollPane;
import menuqac.ui.TextField;
import menuqac.ui.dropdown.DropdownCooking;
import menuqac.ui.dropdown.DropdownSauce;
import menuqac.visitor.CommentVisitor;
import menuqac.visitor.CookingVisitor;
import menuqac.visitor.IceCubesVisitor;
import menuqac.visitor.SauceVisitor;
import menuqac.visitor.SugarVisitor;

public class SaveChangesButton extends Button {
	private static final long serialVersionUID = 1L;

	private Prototype p = null;
	private TextField tf = null;
	private ModifyFrame mf = null;
	private ScrollPane sp = null;
	private CheckBox cb = null;
	private CheckBox cb2 = null;
	private DropdownSauce ds = null;
	private DropdownCooking dc = null;

	private Cooker c = null;
	private PanelActivity pa = null;
	private ParamCooker pc = null;
	private Color color;

	public SaveChangesButton(int x, int y, String text, Prototype p, TextField tf, ModifyFrame mf, ScrollPane sp,
			CheckBox cb, CheckBox cb2, DropdownSauce ds, DropdownCooking dc) {
		super(x, y, 150 - 17, 50, 10, text);
		this.textSize = 15;
		this.p = p;
		this.tf = tf;
		this.mf = mf;
		this.sp = sp;
		this.cb = cb;
		this.cb2 = cb2;
		this.ds = ds;
		this.dc = dc;
		this.color = Color.LIGHT_GRAY;
	}

	public SaveChangesButton(int x, int y, String text, Cooker c, PanelActivity pa, ParamCooker pc) {
		super(x, y, 150 - 17, 50, 10, text);
		this.textSize = 15;
		this.c = c;
		this.pa = pa;
		this.pc = pc;
		this.color = Color.WHITE;
	}

	@Override
	public void paint(Graphics g) {
		g.setColor(this.color);
		g.fillRect(0, 0, this.getWidth(), this.getHeight());
		paintComponent(g);
		paintBorder(g);
	}

	protected void processMouseEvent(MouseEvent e) {
		super.processMouseEvent(e);
		if (e.getID() == MouseEvent.MOUSE_PRESSED) {
			mousePressed = true;
			this.repaint();
		}
		if (e.getID() == MouseEvent.MOUSE_RELEASED) {
			mousePressed = false;
			if (mouseOver) {
				if (tf != null) {
					CommentVisitor cv = new CommentVisitor();
					cv.visit(p, tf.getText());
				}
				if (cb != null) {
					IceCubesVisitor icv = new IceCubesVisitor();
					icv.visit(p, cb.isSelected());
				}
				if (cb2 != null) {
					SugarVisitor sv = new SugarVisitor();
					sv.visit(p, cb2.isSelected());
				}
				if (ds != null) {
					SauceVisitor sav = new SauceVisitor();
					sav.visit(p, ds.getSelectedItem());
				}
				if (dc != null) {
					CookingVisitor cov = new CookingVisitor();
					cov.visit(p, dc.getSelectedItem());
				}
				if (mf != null) {
					mf.dispatchEvent(new WindowEvent(mf, WindowEvent.WINDOW_CLOSING));
				}
				if (sp != null) {
					sp.repaint();
				}
				if (c != null) {
					c.setActivity(pa.getCheckBoxes());
					c.setAFaire();
					c.getPanelToCookAFaire().repaint();
				}
				if (pc != null) {
					pc.dispatchEvent(new WindowEvent(pc, WindowEvent.WINDOW_CLOSING));
				}
			}
			this.repaint();
		}
		if (e.getID() == MouseEvent.MOUSE_ENTERED) {
			mouseOver = true;
			this.repaint();
		}
		if (e.getID() == MouseEvent.MOUSE_EXITED) {
			mouseOver = false;
			this.repaint();
		}
	}
}