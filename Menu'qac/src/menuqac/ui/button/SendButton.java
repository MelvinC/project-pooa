package menuqac.ui.button;

import java.awt.event.MouseEvent;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.Socket;
import menuqac.Order;
import menuqac.Prototype;
import menuqac.ui.Button;
import menuqac.ui.PanelOrder;

public class SendButton extends Button {
	private static final long serialVersionUID = 1L;
	private static final int port = 7777;
	private static final String hostname = "localhost";
	private static final int typeMessage = 1;

	private Order order;
	private PanelOrder panelOrder;

	public SendButton(int x, int y, String text, Order order, PanelOrder panelOrder) {
		super(x, y, text);
		this.order = order;
		this.panelOrder = panelOrder;
	}

	protected void processMouseEvent(MouseEvent e) {
		super.processMouseEvent(e);
		if (e.getID() == MouseEvent.MOUSE_PRESSED) {
			mousePressed = true;
			this.repaint();
		}
		if (e.getID() == MouseEvent.MOUSE_RELEASED) {
			mousePressed = false;
			if (mouseOver) {
				if (this.order.getPrototypes().size() > 0) {
					this.sendOrder(order);
					this.order.empty();
					this.panelOrder.getParent().setPreferredSize(this.panelOrder.getPreferredSize());
					this.panelOrder.getParent().getParent().repaint();
					this.panelOrder.repaint();
				}
			}
			this.repaint();
		}
		if (e.getID() == MouseEvent.MOUSE_ENTERED) {
			mouseOver = true;
			this.repaint();
		}
		if (e.getID() == MouseEvent.MOUSE_EXITED) {
			mouseOver = false;
			this.repaint();
		}
	}

	private void sendOrder(Order o) {
		Order or = new Order(o.getNumber(), o.getWaiter(), o.getTableNumber());
		for (Prototype p : o.getPrototypes()) {
			for (int i = 0; i < p.getQuantity(); i++) {
				or.add(p.clone());
			}
			p.setQuantity(1);
		}
		try {
			Socket sock = new Socket(SendButton.hostname, SendButton.port);
			ObjectOutputStream oos = new ObjectOutputStream(sock.getOutputStream());
			oos.writeInt(SendButton.typeMessage);
			oos.writeObject(or);
			sock.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}