package menuqac.ui.dropdown;

import javax.swing.plaf.basic.BasicArrowButton;

public class ArrowButton extends BasicArrowButton {
	private static final long serialVersionUID = 1L;

	public ArrowButton() {
		super(BasicArrowButton.SOUTH);
		this.setVisible(false);
	}
}