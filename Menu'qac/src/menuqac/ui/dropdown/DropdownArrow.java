package menuqac.ui.dropdown;

import javax.swing.JButton;
import javax.swing.plaf.ComboBoxUI;
import javax.swing.plaf.basic.BasicComboBoxUI;

public class DropdownArrow extends BasicComboBoxUI {
	public static ComboBoxUI createUI() {
		return new DropdownArrow();
	}

	@Override
	protected JButton createArrowButton() {
		return new ArrowButton();
	}
}