package menuqac.ui.dropdown;

import java.util.ArrayList;
import menuqac.Table;
import menuqac.ui.CashRegister;
import menuqac.ui.CashRegisterItemChangeListener;
import menuqac.ui.Dropdown;

public class DropdownCashRegister extends Dropdown {
	private static final long serialVersionUID = 1L;

	public DropdownCashRegister(int x, int y, ArrayList<Table> tables, CashRegister cashRegister) {
		super(x, y, tables);
		this.addItemListener(new CashRegisterItemChangeListener(cashRegister));
	}

	public DropdownCashRegister(int x, int y, int w, int h, ArrayList<Table> tables, CashRegister cashRegister) {
		super(x, y, w, h, tables);
		this.addItemListener(new CashRegisterItemChangeListener(cashRegister));
	}
}