package menuqac.ui.dropdown;

import java.awt.Color;
import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.ListCellRenderer;
import menuqac.visitor.CookingVisitor.Cooking;

public class DropdownCookingItemRenderer extends JPanel implements ListCellRenderer<Cooking> {
	private static final long serialVersionUID = 1L;

	private DropdownRenderer labelItem = new DropdownRenderer();

	protected Color normalColor;
	protected Color pressedColor;
	protected Color overColor;

	public DropdownCookingItemRenderer(DropdownCooking d) {
		setLayout(new GridBagLayout());
		GridBagConstraints constraints = new GridBagConstraints();
		constraints.fill = GridBagConstraints.HORIZONTAL;
		constraints.weightx = 1.0;
		constraints.insets = new Insets(0, 0, 0, 0);
		labelItem.setOpaque(true);
		labelItem.setHorizontalAlignment(JLabel.LEFT);
		add(labelItem, constraints);
		setBackground(Color.WHITE);
	}

	@Override
	public Component getListCellRendererComponent(JList<? extends Cooking> list, Cooking value, int index,
			boolean isSelected, boolean cellHasFocus) {
		labelItem.setText(value.toString());
		if (isSelected) {
			labelItem.setOver(true);
		} else {
			labelItem.setOver(false);
		}
		this.repaint();
		return this;
	}
}