package menuqac.ui.dropdown;

import java.awt.Color;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.Font;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.plaf.basic.BasicComboBoxEditor;
import menuqac.Table;

public class DropdownEditor extends BasicComboBoxEditor {
	private JLabel label = new JLabel();
	private JPanel panel = new JPanel();
	private Object selectedItem;

	public DropdownEditor(DropdownWaiter dropdown) {
		label.setOpaque(false);
		label.setFont(new Font("Dialog", Font.BOLD, dropdown.getHeight() / 2));
		label.setForeground(Color.WHITE);
		panel.setLayout(new FlowLayout(FlowLayout.CENTER, 0, 5));
		panel.add(label);
		panel.setBackground(new Color(103, 130, 58));
	}

	public Component getEditorComponent() {
		return this.panel;
	}

	public Object getItem() {
		return this.selectedItem.toString();
	}

	public void setItem(Table item) {
		this.selectedItem = item;
		label.setText(item.toString());
	}
}