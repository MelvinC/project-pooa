package menuqac.ui.dropdown;

import java.awt.Color;
import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.ListCellRenderer;
import menuqac.Table;
import menuqac.ui.Dropdown;

public class DropdownItemRenderer extends JPanel implements ListCellRenderer<Table> {
	private static final long serialVersionUID = 1L;

	private DropdownRenderer labelItem = new DropdownRenderer();

	protected Color normalColor;
	protected Color pressedColor;
	protected Color overColor;

	public DropdownItemRenderer(Dropdown d) {
		setLayout(new GridBagLayout());
		GridBagConstraints constraints = new GridBagConstraints();
		constraints.fill = GridBagConstraints.HORIZONTAL;
		constraints.weightx = 1.0;
		constraints.insets = new Insets(0, 0, 0, 0);
		labelItem.setOpaque(true);
		labelItem.setHorizontalAlignment(JLabel.LEFT);
		add(labelItem, constraints);
		setBackground(Color.WHITE);
	}

	@Override
	public Component getListCellRendererComponent(JList<? extends Table> list, Table value, int index,
			boolean isSelected, boolean cellHasFocus) {
		labelItem.setText(value.toString());
		if (isSelected) {
			labelItem.setOver(true);
		} else {
			labelItem.setOver(false);
		}
		this.repaint();
		return this;
	}
}