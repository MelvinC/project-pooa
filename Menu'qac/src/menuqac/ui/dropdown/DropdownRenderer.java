package menuqac.ui.dropdown;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.MouseEvent;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.ListCellRenderer;
import menuqac.Table;

public class DropdownRenderer extends JLabel implements ListCellRenderer<Table> {
	private static final long serialVersionUID = 1L;

	protected Color borderColor;
	protected Color normalColor;
	protected Color pressedColor;
	protected Color overColor;

	private Color textNormalColor;
	private Color textPressedColor;
	private Color textOverColor;
	private String fontName;

	protected boolean mousePressed = false;
	protected boolean mouseOver = false;

	public DropdownRenderer() {
		this.setPreferredSize(new Dimension(500, 25));
		this.borderColor = Color.BLACK;
		this.normalColor = Color.WHITE;
		this.pressedColor = new Color(85, 58, 130);
		this.overColor = new Color(103, 130, 58);
		this.textNormalColor = new Color(103, 130, 58);
		this.textPressedColor = Color.WHITE;
		this.textOverColor = Color.WHITE;
		this.fontName = "Dialog";
	}

	protected void processMouseEvent(MouseEvent e) {
		super.processMouseEvent(e);
		if (e.getID() == MouseEvent.MOUSE_PRESSED) {
			mousePressed = true;
			this.repaint();
		}
		if (e.getID() == MouseEvent.MOUSE_RELEASED) {
			mousePressed = false;
			this.repaint();
		}
		if (e.getID() == MouseEvent.MOUSE_ENTERED) {
			mouseOver = true;
			this.repaint();
		}
		if (e.getID() == MouseEvent.MOUSE_EXITED) {
			mouseOver = false;
			this.repaint();
		}
	}

	@Override
	public void paint(Graphics g) {
		paintComponent(g);
	}

	@Override
	protected void paintComponent(Graphics g) {
		if (mousePressed) {
			g.setColor(this.pressedColor);
		} else {
			if (mouseOver) {
				g.setColor(this.overColor);
			} else {
				g.setColor(this.normalColor);
			}
		}
		g.fillRect(0, 0, this.getWidth(), this.getHeight());
		if (mousePressed) {
			g.setColor(this.textPressedColor);
		} else {
			if (mouseOver) {
				g.setColor(this.textOverColor);
			} else {
				g.setColor(this.textNormalColor);
			}
		}
		g.setFont(new Font(this.fontName, Font.PLAIN, 12));
		g.drawString(this.getText(), 8, 17);
	}

	public void setOver(boolean b) {
		mouseOver = b;
		this.repaint();
	}

	@Override
	public Component getListCellRendererComponent(JList<? extends Table> list, Table value, int index,
			boolean isSelected, boolean cellHasFocus) {
		this.setText(value.toString());
		return null;
	}
}