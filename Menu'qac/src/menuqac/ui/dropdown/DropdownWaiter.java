package menuqac.ui.dropdown;

import java.util.ArrayList;
import menuqac.Order;
import menuqac.Table;
import menuqac.ui.Dropdown;
import menuqac.ui.WaiterItemChangeListener;

public class DropdownWaiter extends Dropdown {
	private static final long serialVersionUID = 1L;

	public DropdownWaiter(int x, int y, ArrayList<Table> tables, Order order) {
		super(x, y, tables);
		this.addItemListener(new WaiterItemChangeListener(order, this));
	}

	public DropdownWaiter(int x, int y, int w, int h, ArrayList<Table> tables, Order order) {
		super(x, y, w, h, tables);
		this.addItemListener(new WaiterItemChangeListener(order, this));
	}
}