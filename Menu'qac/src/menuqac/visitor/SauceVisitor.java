package menuqac.visitor;

import menuqac.Prototype;
import menuqac.Visitor;

public class SauceVisitor implements Visitor {
	public enum Sauce {
		NONE, KETCHUP, MAYO, MUSTARD, BARBECUE
	}

	@Override
	public void visit(Prototype p, Object value) {
		p.accepts(this, (Sauce) value);
	}
}